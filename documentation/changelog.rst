..
 # Copyright (c) 2022-2023, Arm Limited.
 #
 # SPDX-License-Identifier: MIT

#########################
Changelog & Release Notes
#########################

.. _Release 5.0 - InterVM Communication with Linux Containers:

*********************************************************
Release 5.0 - InterVM Communication with Linux Containers
*********************************************************

New Features
============

  * Provided static shared memory in Xen hypervisor allowing users to statically
    set up shared memory on a dom0less system, enabling domains to do shm-based
    communication
  * Provided static event channel established in Xen hypervisor for event
    notification between two domains
  * Implemented Inter-VM communication using RPMsg protocol provided by the
    OpenAMP framework together with the static shared memory and static event
    channel provided by Xen hypervisor
  * Added docker support in the Linux domain, showing standard application
    deployments via containers
  * Added a docker container hosted Nginx web server to serve data for external
    users visiting through the HTTP protocol
  * Added Yocto testimage support
  * Added test suite that can validate the functionalities in Baremetal Zephyr
  * Enhanced the test suite to validate Baremetal Linux and Virtualization stack

Changed
=======

  * Upgraded to FVP version 11.20.15
  * Upgraded Yocto to langdale
  * Upgraded to U-Boot 2022.07
  * Upgraded to Xen 4.17
  * Upgraded to Zephyr 3.2.0
  * Upgraded to Linux kernel 5.19
  * Enabled ``CONFIG_XEN`` options for Linux domain to add Xen kernel support
  * Changed to use the meta-zephyr Yocto layer to build Zephyr applications

    .. code-block:: shell

       URL: https://git.yoctoproject.org/meta-zephyr
       layers: meta-zephyr-core
       branch: langdale
       revision: 375aea434ef214a5022e0a3e54fcae3e5e965c9b

  * Introduced the meta-openamp Yocto layer to build libmetal and OpenAMP
    libraries

    .. code-block:: shell

       URL: https://github.com/OpenAMP/meta-openamp
       layers: meta-openamp
       |inclusivity-exception| branch: master
       revision: cd5cc9274321ea9d808a7800cfcba26320cf53a5

  * Other third-party Yocto layers used to build the Software Stack:

    .. code-block:: shell

       URL: https://git.yoctoproject.org/git/poky
       layers: meta, meta-poky
       branch: langdale
       revision: a3e3b740e140d036122f7b11e2ac452bda548444

       URL: https://git.openembedded.org/meta-openembedded
       layers: meta-filesystems, meta-networking, meta-oe, meta-python
       branch: langdale
       revision: c354f92778c1d4bcd3680af7e0fb0d1414de2344

       URL: https://git.yoctoproject.org/git/meta-virtualization
       layers: meta-virtualization
       branch: langdale
       revision: 8857b36ebfec3d548755755b009adc491ef320ab

       URL: https://git.yoctoproject.org/git/meta-arm
       layers: meta-arm, meta-arm-bsp, meta-arm-toolchain
       branch: langdale
       revision: 025124814e8676e46d42ec5b07220283f1bdbcd0

Known Issues and Limitations
============================

  * Xen ``shared-memory`` does not support ``xen,offset`` yet, which is
    introduced in the Linux documentation at
    `Xen hypervisor reserved-memory binding`_
  * The below feature for Xen static shared memory device node "xen,shared-mem"
    is not implemented yet:

        Host physical address is optional, when missing Xen decides the location

  * The limitations of :ref:`RPMsg Demo` mentioned in
    :ref:`Limitations and Improvements`
  * The ``bp.refcounter.use_real_time`` model parameter is not supported for the
    hypervisor and baremetal Zephyr use cases
  * Issues and limitations mentioned in `Known Issues and Limitations of Release 2`_

.. _Xen hypervisor reserved-memory binding: https://www.kernel.org/doc/Documentation/devicetree/bindings/reserved-memory/xen%2Cshared-memory.txt

.. _Release 4.0 - Xen:

*****************
Release 4.0 - Xen
*****************

New Features
============

  * Added ability to boot one Linux and one Zephyr domain on Xen in dom0less
    mode
  * Added ability to boot three Zephyr sample applications
  * Added basic Linux test suite using BATS

Changed
=======

  * A new Yocto layer has been created called ``meta-armv8r64-extras``, which
    extends ``meta-arm-bsp`` with additional use cases
  * Moved documentation to the new repository and expanded to cover new use
    cases. It is now published on *ReadTheDocs*
  * Upgraded to FVP version 11.18.16
  * Upgraded to U-Boot 2022.01 and enabled support for applying device tree
    overlays
  * Upgraded to Linux kernel 5.15
  * Added support to boot-wrapper-aarch64 for S-EL2 SMP payloads such as Xen
  * Amended the device tree to support the virtio random number generator
  * Added *ssh-pregen-hostkeys* to the default image to improve boot times
  * Amended the default model parameters to support enabling
    ``cache_state_modelled``
  * Removed support for PREEMPT_RT Linux kernel builds
  * Third-party Yocto layers used to build the Software Stack:

    .. code-block:: shell

       URL: https://git.yoctoproject.org/git/poky
       layers: meta, meta-poky
       branch: kirkstone
       revision: 0c3b92901cedab926f313a2b783ff2e8833c95cf

       URL: https://git.openembedded.org/meta-openembedded
       layers: meta-filesystems, meta-networking, meta-oe, meta-python
       branch: kirkstone
       revision: fcc7d7eae82be4c180f2e8fa3db90a8ab3be07b7

       URL: https://git.yoctoproject.org/git/meta-virtualization
       layers: meta-virtualization
       branch: kirkstone
       revision: 0d35c194351a9672d18bff52a8f2fbabcd5b0f3d

       URL: https://git.yoctoproject.org/git/meta-arm
       layers: meta-arm, meta-arm-bsp, meta-arm-toolchain
       branch: kirkstone
       revision: 32ca791aaa6634e90a7c6ea3994189ef10a7ac90

Known Issues and Limitations
============================

  * The ``bp.refcounter.use_real_time`` model parameter is not supported for the
    hypervisor and baremetal Zephyr use cases
  * "Dom0less" is a new set of features for Xen, some of which are still being
    developed in the Xen community. In this case, a "dom0less" domain running
    Linux cannot enable the ``CONFIG_XEN`` option, which will cause some
    limitations. For example, Linux can't detect that it is running inside a Xen
    domain, so some Xen domain platform-specific Power Management control paths
    will not be invoked. One of the specific cases is that using the command
    ``echo 0 > /sys/devices/system/cpu/cpu0/online`` to power off the Linux
    domain's vcpu0 is invalid.
  * Issues and limitations mentioned in `Known Issues and Limitations of Release 2`_

****************
Release 3 - UEFI
****************

Release Note
============

https://community.arm.com/oss-platforms/w/docs/638/release-3---uefi

New Features
============

  * Added U-Boot v2021.07 for UEFI support
  * Updated boot-wrapper-aarch64 revision and added support for booting U-Boot
  * Included boot-wrapper-aarch64 PSCI services in ``/memreserve/`` region

Changed
=======

  * Configured the FVP to use the default RAM size of 4 Gb
  * Added virtio_net User Networking mode by default and removed instructions
    about tap networking setup
  * Updated Linux kernel version from 5.10 to 5.14 for both standard and
    Real-Time (PREEMPT_RT) builds
  * Fixed the counter frequency initialization in boot-wrapper-aarch64
  * Fixed ``PL011`` and ``SP805`` register sizes in the device tree

Known Issues and Limitations
============================

  * Device DMA memory cache-coherence issue: the FVP ``cache_state_modelled``
    parameter will affect the cache coherence behavior of peripherals’ DMA. When
    users set ``cache_state_modelled=1``, they also have to set
    ``cci400.force_on_from_start=1`` to force the FVP to enable snooping on
    upstream ports
  * Issues and limitations mentioned in `Known Issues and Limitations of Release 2`_

***************
Release 2 - SMP
***************

Release Note
============

https://community.arm.com/oss-platforms/w/docs/634/release-2---smp

New Features
============

  * Enabled SMP support via boot-wrapper-aarch64 providing the PSCI ``CPU_ON``
    and ``CPU_OFF`` functions
  * Introduced Armv8-R64 compiler flags
  * Added Linux PREEMPT_RT support via linux-yocto-rt-5.10
  * Added support for file sharing with the host machine using Virtio P9
  * Added support for runfvp
  * Added performance event support (PMU) in the Linux device tree

Changed
=======

None.

.. _Known Issues and Limitations of Release 2:

Known Issues and Limitations
============================

  * Only PSCI ``CPU_ON`` and ``CPU_OFF`` functions are supported
  * Linux kernel does not support booting from secure EL2 on Armv8-R AArch64
  * Linux KVM does not support Armv8-R AArch64

***********************
Release 1 - Single Core
***********************

Release Note
============

https://community.arm.com/oss-platforms/w/docs/633/release-1-single-core

New Features
============

Introduced fvp-baser-aemv8r64 as a Yocto machine support the following BSP
components on the Yocto hardknott release branch, where a standard Linux kernel
can be built and run:

  * boot-wrapper-aarch64
  * Linux kernel: linux-yocto-5.10

Changed
=======

Initial version.

Known Issues and Limitations
============================

  * Only support one CPU since SMP is not functional in boot-wrapper-aarch64 yet
