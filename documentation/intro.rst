..
 # Copyright (c) 2022, Arm Limited.
 #
 # SPDX-License-Identifier: MIT

.. _Introduction:

############
Introduction
############

.. _Overview:

********
Overview
********

The **Armv8-R AArch64 Software Stack** (referred to as **Software Stack**
hereafter) provides a collection of software examples intended to run on the
Armv8-R AEM FVP [1]_, which is an implementation of the Armv8-R AArch64
architecture [2]_. Together, they are intended to enable exploration of the
general Armv8-R AArch64 architecture as well as a specific implementation -
|Cortex|-R82 [3]_.

The software provides example deployments of two classes of operating system;
Linux as a Rich OS, and Zephyr as an RTOS (Real-Time Operating System) and
either can run as bare-metal or under virtualization provided by Xen as a Type-1
hypervisor.

The Poky Linux distribution provides a platform for hosting and deploying user
applications with OCI (Open Container Initiative) compliant containers (via
Docker) as well as network connectivity. Additionally, the Xen hypervisor
provides a communication path for data transfer between the VM guests.

The Yocto layers which define the project contain all the necessary instructions
to fetch and build the source, as well as to download the FVP model and launch
the examples.

Fixed Virtual Platforms (FVP) are complete simulations of an Arm system,
including processor, memory and peripherals. These are set out in a
"programmer's view", which gives you a comprehensive model on which to build and
test your software. The Fast Models FVP Reference Guide [4]_ provides the
necessary details. The Armv8-R AEM FVP is a free of charge Armv8-R Fixed Virtual
Platform. It supports the latest Armv8-R feature set.

This document describes how to build and run the Armv8-R AArch64 Software Stack
on the Armv8-R AEM FVP (AArch64) platform. It also covers how to extend features
and configurations based on the Software Stack.

.. _Audience:

********
Audience
********

This document is intended for software, hardware, and system engineers who are
planning to use the Armv8-R AArch64 Software Stack, and for anyone who wants to
know more about it.

This document describes how to build an image for Armv8-R AArch64 using a Yocto
Project [5]_ build environment. Basic instructions can be found in the Yocto
Project Quick Start [6]_ document.

In addition to having Yocto related knowledge, the target audience also need to
have a certain understanding of the following components:

  * |Arm| architectures [7]_
  * Bootloader (boot-wrapper-aarch64 [8]_ and U-Boot [9]_)
  * Linux kernel [10]_
  * Zephyr [11]_
  * Virtualization technology and Xen [12]_ hypervisor

.. _Use Cases Overview:

******************
Use Cases Overview
******************

There are 3 sub-stacks supported in the Software Stack for either baremetal
solution or virtualization solution separately:

  * Baremetal solution (without hardware virtualization support)

    * **Baremetal Linux** stack

      Boot a Linux based Rich OS (via U-Boot with UEFI) to command prompt

    * **Baremetal Zephyr** Stack

      Boot Zephyr RTOS directly and run a sample application

  * Virtualization solution

    * **Virtualization** stack

      Boot the Xen hypervisor (via U-Boot without UEFI) and start two isolated
      domains to run Linux Rich OS and Zephyr RTOS in parallel on the fixed
      number of cores statically allocated by the Xen hypervisor.

      In this stack, Xen provides static shared memory and static event
      channel support so that the Linux and Zephyr domains running on it can
      communicate with each other using the RPMsg (Remote Processor Messaging)
      protocol implemented in the OpenAMP (Open Asymmetric Multi-Processing)
      framework.

      In addition, the Linux domain can also run a container hosted Nginx web
      server to serve data for external users visiting through the HTTP
      protocol.

Instructions for achieving these use cases are given in the article
:doc:`user-guide/reproduce`, subject to the relevant assumed technical
knowledge as listed at the `Audience`_ section in this document.

.. _High Level Architecture:

***********************
High Level Architecture
***********************

The high-level architecture corresponding to the three use cases (as described
in the `Use Cases Overview`_ section above) supported by this Software Stack is
as follows:

The diagram below gives an overview of the components and layers in the
implementation of Virtualization stack with Xen hypervisor, adding the main
modules required to implement the following two scenarios:

  * Inter-VM communication with each other between the two domains running on
    the Xen hypervisor
  * Exposing data with a docker container hosted Nginx web server

Inter-VM communication is implemented using the OpenAMP framework in the
application layer, and supported by the static shared memory and static event
channel mechanism provided by Xen in the backend.

.. image:: images/armv8r64-virtualization-with-inter-vm-communication-and-docker.png
   :alt: Arm v8r64 architecture hypervisor with inter-VM communication and docker
   :align: center

|

The diagram below gives an overview of the components and layers in the
implementation of Virtualization stack.

.. image:: images/armv8r64-virtualization-refstack-arch.png
   :alt: Arm v8r64 architecture hypervisor
   :align: center

|

The diagram below gives an overview of the components and layers in the
Baremetal Linux stack.

.. image:: images/armv8r64-baremetal-refstack-arch.png
   :alt: Arm v8r64 architecture Linux
   :align: center

|

The diagram below gives an overview of the components and layers in the
Baremetal Zephyr stack.

.. image:: images/armv8r64-baremetal-zephyr-refstack-arch.png
   :alt: Arm v8r64 architecture Zephyr
   :align: center

|


*****************
Features Overview
*****************

This Software Stack implements the following major features at system level:

  * A Zephyr RTOS running on baremetal supporting SMP (Symmetric
    Multi-Processing)
  * A standard Linux Rich OS running on baremetal supporting the following
    functions:

    * Booting Linux kernel using U-Boot UEFI (Unified Extensible Firmware
      Interface) and Grub2
    * SMP (Symmetric Multi-Processing)
    * Test suite that can validate the basic functionalities

  * A dual-OS system - Zephyr as the RTOS and Linux as the Rich OS - running in
    parallel using the Xen hypervisor, with supporting the following functions:

    * Booting dom0less Xen from U-Boot
    * SMP in Xen hypervisor and the OSes (both Linux and Zephyr) running in Xen
      domains
    * Inter-VM communication using RPMsg protocol provided by the OpenAMP
      framework together with the static shared memory and static event channel
      provided by Xen
    * Docker in the Linux domain, showing standard application deployments via
      containers
    * A docker container hosted Nginx web server to serve data for external
      users visiting through the HTTP protocol
    * Test suite that can validate the functionalities in Zephyr and Linux, as
      well as the communication between the two OSes and docker running in the
      Linux domain
    * Classic sample applications in Zephyr

Some other features at component level include:

  * Device tree overlay to apply changes on the fly (provided by U-Boot)
  * Device pass through support for virtio-net, virtio-blk, virtio-rng,
    virtio-9p and virtio-rtc (provided by Xen to the Linux kernel)
  * PSCI service (provided by boot-wrapper-aarch64)
  * Boot from S-EL2 (provided by boot-wrapper-aarch64, U-Boot and Xen)
  * Boot-wrapper-aarch64 (as an alternative solution to Trusted-Firmware) and
    hypervisor (Xen) co-existence in S-EL2 at runtime
  * Static shared memory in Xen hypervisor allowing users to statically set up
    shared memory on a dom0less system, enabling domains to do shm-based
    communication
  * Static event channel established in Xen hypervisor for event notification
    between two domains

**********************
Documentation Overview
**********************

The documentation is structured as follows:

* :doc:`user-guide/index`

  Provides the guide to setup environment, build and run the Software Stack on
  the target platform, run the provided test suite and validate supported
  functionalities. Also includes the guidance for how to use extra features,
  customize configurations, and reuse the components in this Software Stack.

* :doc:`manual/index`

  Provides more advanced developer-focused details about this Software Stack,
  include its implementation, and dependencies, etc.

* :doc:`codeline_management`

  Describes the Armv8-R AArch64 Software Stack's branches and release process.

* :doc:`license_link`

  Defines the license under which this Software Stack is provided.

* :doc:`changelog`

  Documents new features, bug fixes, known issues and limitations, and any other
  changes provided under each release.

********************
Repository Structure
********************

The Armv8-R AArch64 Software Stack is provided through the v8r64 repository
[13]_, which is structured as follows:

  * ``meta-armv8r64-extras``

    Yocto layer with the target platform (fvp-baser-aemv8r64 [14]_) extensions
    for Armv8-R AArch64. This layer extends the meta-arm-bsp [15]_ layer in the
    meta-arm [16]_ repository. It aims to release the updated and new Yocto
    Project recipes and machine configurations for Armv8-R AArch64 that are not
    available in the existing meta-arm and meta-arm-bsp layers in the Yocto
    Project. This layer also provides image recipes that include all the
    components needed for a system image to boot, making it easier for the
    user. Components can be built individually or through an image recipe, which
    pulls all the components required in an image into one build process.

  * ``documentation``

    Directory which contains the documentation sources, defined in
    reStructuredText (.rst) format for rendering via Sphinx [17]_.

  * ``config``

    Directory which contains configuration files for running tools on the v8r64
    repository, such as files for running automated quality-assurance checks.

  * ``components``

    Directory containing source code for components which can either be used
    directly or as part of the Yocto BSP layer or the Yocto meta-armv8r64-extras
    layer.

The Yocto layers which are provided by meta-armv8r64-extras are detailed with
the layer structure and dependencies in :doc:`manual/yocto-layers`.

******************
Repository License
******************

The repository's standard license is the MIT license (more details in
:doc:`license_link`), under which most of the repository's content is
provided. Exceptions to this standard license relate to files that represent
modifications to externally licensed works (for example, patch files). These
files may therefore be included in the repository under alternative licenses in
order to be compliant with the licensing requirements of the associated external
works.

Contributions to the project should follow the same licensing arrangement.

***************
Issue Reporting
***************

To report issues with the repository such as potential bugs, security concerns,
or feature requests, please submit an Issue via |GitLab Issues|_, following the
project's Issue template.

********************
Feedback and Support
********************

To request support please contact Arm at support@arm.com. Arm licensees may also
contact with Arm via their partner managers.

*************
Maintainer(s)
*************

  * Robbie Cao <robbie.cao@arm.com>

*********
Reference
*********

.. [1] https://developer.arm.com/tools-and-software/simulation-models/fixed-virtual-platforms/arm-ecosystem-models

.. [2] https://developer.arm.com/documentation/ddi0600/ac

.. [3] https://developer.arm.com/Processors/Cortex-R82

.. [4] https://developer.arm.com/docs/100966/latest

.. [5] https://www.yoctoproject.org/

.. [6] https://docs.yoctoproject.org/brief-yoctoprojectqs/index.html

.. [7] https://developer.arm.com/architectures

.. [8] https://git.kernel.org/pub/scm/linux/kernel/git/mark/boot-wrapper-aarch64.git/

.. [9] https://www.denx.de/wiki/U-Boot

.. [10] https://www.kernel.org/

.. [11] https://www.zephyrproject.org/

.. [12] https://xenproject.org/

.. [13] https://gitlab.arm.com/automotive-and-industrial/v8r64

.. [14] https://git.yoctoproject.org/meta-arm/tree/meta-arm-bsp/documentation/fvp-baser-aemv8r64.md

.. [15] https://git.yoctoproject.org/meta-arm/tree/meta-arm-bsp?h=langdale

.. [16] https://git.yoctoproject.org/meta-arm/

.. [17] https://www.sphinx-doc.org/
