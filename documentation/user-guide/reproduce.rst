..
 # Copyright (c) 2022-2023, Arm Limited.
 #
 # SPDX-License-Identifier: MIT

.. _Reproduce:

#########
Reproduce
#########

This document provides the instructions for setting up the build environment,
checking out code, building, running and validating the key use cases.

*****************
Environment Setup
*****************

The following instructions have been tested on hosts running Ubuntu 20.04.
Install the required packages for the build host:
https://docs.yoctoproject.org/4.1.1/singleindex.html#required-packages-for-the-build-host

Kas is a setup tool for bitbake based projects. The minimal supported version is
``3.0.2`` (version ``3.1`` was used during the development), install it like so:

.. code-block:: shell

   pip3 install --user --upgrade kas==3.1

For more details on kas, see https://kas.readthedocs.io/.

To build the recipe for the FVP_Base_AEMv8R model itself, you also need to
accept the EULA [1]_ by setting the following environment variable:

.. code-block:: shell

   export FVP_BASE_R_ARM_EULA_ACCEPT="True"


To fetch and build the ongoing development of this Software Stack,  follow the
instructions in the below sections of this document.

.. note::

   The host machine should have at least 85 GBytes of free disk space for the
   next steps to work correctly.

********
Download
********

Fetch the v8r64 repository into a build directory:

.. code-block:: shell

   mkdir -p ~/fvp-baser-aemv8r64-build
   cd ~/fvp-baser-aemv8r64-build
   git clone https://gitlab.arm.com/automotive-and-industrial/v8r64 -b v5.0

.. _Build and Run:

*************
Build and Run
*************

The Software Stack supports building and running three sub-stacks: Baremetal
Zephyr, Baremetal Linux and Virtualization, which are associated with the use
cases described in :ref:`Use Cases Overview`. Instructions to build and run
these three sub-stacks are as below:

Baremetal Zephyr
================

Build and run with the below commands:

.. code-block:: shell

   cd ~/fvp-baser-aemv8r64-build

   # Build
   kas build v8r64/meta-armv8r64-extras/kas/baremetal-zephyr.yml \
       --target zephyr-synchronization

   # Output images will be located at
   # build/tmp_baremetal-zephyr/deploy/images/fvp-baser-aemv8r64/

   # Run
   kas shell -k v8r64/meta-armv8r64-extras/kas/baremetal-zephyr.yml \
       -c "../layers/meta-arm/scripts/runfvp --verbose --console"

To finish the FVP emulation, you need to close the ``telnet`` session:

  1. Escape to telnet console with ``ctrl+]``.
  2. Run ``quit`` to close the session.

A different sample application can be selected by changing the Kas ``--target``
flag value, e.g.:

.. code-block:: shell

   kas build v8r64/meta-armv8r64-extras/kas/baremetal-zephyr.yml \
       --target zephyr-helloworld

There are 3 supported targets ``zephyr-helloworld``, ``zephyr-synchronization``
and ``zephyr-philosophers``. See the :ref:`Zephyr` section for more information.

Baremetal Linux
===============

Build and run with the below commands:

.. code-block:: shell

   cd ~/fvp-baser-aemv8r64-build

   # Build
   kas build v8r64/meta-armv8r64-extras/kas/baremetal-linux.yml

   # Output images will be located at
   # build/tmp_baremetal-linux/deploy/images/fvp-baser-aemv8r64/

   # Run
   kas shell -k v8r64/meta-armv8r64-extras/kas/baremetal-linux.yml \
       -c "../layers/meta-arm/scripts/runfvp --verbose --console"

.. _Virtualization:

Virtualization
==============

Build and run with the below commands:

.. code-block:: shell

   cd ~/fvp-baser-aemv8r64-build

   # Build
   kas build v8r64/meta-armv8r64-extras/kas/virtualization.yml

   # Output images are located at
   # build/tmp_virtualization/deploy/images/fvp-baser-aemv8r64/

   # Run
   kas shell -k v8r64/meta-armv8r64-extras/kas/virtualization.yml \
       -c "../layers/meta-arm/scripts/runfvp --verbose"

   # Check Xen output.
   telnet localhost 5000

   # Check guest OS in another terminals after Xen started
   # Zephyr
   # The zephyr-rpmsg-demo application starts automatically.
   telnet localhost 5001

   # Linux
   # The rpmsg-demo application starts automatically.
   telnet localhost 5002

.. note::

   When ``--console`` is specified, port ``5002`` (which exposes the Linux
   terminal) is automatically connected through ``telnet``. In this case, please
   do not connect to port 5002 again, as another connection to port ``5002``
   terminates the FVP. If no Linux log is printed, connect to port ``5000``
   (which exposes the Xen terminal) in another terminal. More details of
   ``runfvp`` can be found at `runfvp.md`_.

.. _runfvp.md: https://git.yoctoproject.org/meta-arm/tree/documentation/runfvp.md?h=langdale

.. note::

   When running the ``runfvp`` command with the ``--verbose`` option enabled,
   you will see the following output:

   .. code-block:: shell

      terminal_0: Listening for serial connection on port 5000
      terminal_1: Listening for serial connection on port 5001
      terminal_2: Listening for serial connection on port 5002
      terminal_3: Listening for serial connection on port 5003

   Among them, port ``5000`` is assigned to the Xen hypervisor, ``5001`` to the
   Zephyr domain, ``5002`` to the Linux domain, and ``5003`` unused. If these
   ports are already occupied (for example, there is already a ``runfvp``
   instance running), then the port number will automatically increase by 4,
   that is, ports ``5004`` ~ ``5007`` will be assigned. In this case, checking
   the output from the Zephyr and Linux domains requires using ports ``5005``
   and ``5006``:

   .. code-block:: shell

      telnet localhost 5005
      telnet localhost 5006

   If any port(s) in ``5000`` ~ ``5003`` is (are) used by other programs, the
   FVP will try to find 4 available ports starting from ``5000``. Be sure to
   check the log of ``runfvp`` (with ``--verbose``) to determine the correct
   port numbers, then apply the determined port number to the ``telnet``
   command.

By default, the ``rpmsg-demo`` application and the Nginx web server start
automatically. ``rpmsg-demo`` sends status data from the Zephyr domain to the
Linux domain, and saves the data to :file:`zephyr-status.html`. The files can be
visited through Nginx web server using the following commands in the Linux
domain.

.. code-block:: shell

   # The Nginx docker container starts automatically in Linux.
   # Check that Nginx is working in Linux.
   wget http://localhost/index.html

   # Check the rpmsg-demo output in Linux. rpmsg-demo updates this file
   # periodically.
   wget http://localhost/zephyr-status.html

These pages can also be visited from a web browser in the host that is running
FVP. By default port ``80`` in FVP is mapped to port ``8080`` on host in the
Virtualization stack. To browse these pages in the host web browser, use
http://localhost:8080/index.html and http://localhost:8080/zephyr-status.html
respectively.

More details of :ref:`Demo Applications` can be found in :ref:`Applications`
section.

.. _Validate:

********
Validate
********

The Software Stack contains test suite which are enabled using the environment
variable ``TESTIMAGE_AUTO=1``. They can be included into the target build to
automatically validate the functionality of each stack. The test cases use the
Yocto image testing feature [2]_ to boot the FVP and validate the output.

Baremetal Zephyr
================

For the Zephyr RTOS, tests are provided to validate the three sample
applications supported in this Software Stack. Use the following command to
build and run the tests:

.. code-block:: shell

   TESTIMAGE_AUTO=1 kas build v8r64/meta-armv8r64-extras/kas/baremetal-zephyr.yml


An example of the test suite output is as follows:

.. code-block:: shell

   Creating terminal default on terminal_0
   Skipping - not zephyr-helloworld
   Skipping - not zephyr-philosophers
   RESULTS:
   RESULTS - zephyr_v8r64.ZephyrTests.test_synchronization: PASSED (3.79s)
   RESULTS - zephyr_v8r64.ZephyrTests.test_helloworld: SKIPPED (0.00s)
   RESULTS - zephyr_v8r64.ZephyrTests.test_philosophers: SKIPPED (0.00s)
   SUMMARY:
   zephyr-synchronization () - Ran 3 tests in 3.793s
   zephyr-synchronization - OK - All required tests passed (successes=1, skipped=2, failures=0, errors=0)
   Creating terminal default on terminal_0
   Skipping - not zephyr-helloworld
   Skipping - not zephyr-synchronization
   RESULTS:
   RESULTS - zephyr_v8r64.ZephyrTests.test_philosophers: PASSED (0.64s)
   RESULTS - zephyr_v8r64.ZephyrTests.test_helloworld: SKIPPED (0.00s)
   RESULTS - zephyr_v8r64.ZephyrTests.test_synchronization: SKIPPED (0.00s)
   SUMMARY:
   zephyr-philosophers () - Ran 3 tests in 0.638s
   zephyr-philosophers - OK - All required tests passed (successes=1, skipped=2, failures=0, errors=0)
   Creating terminal default on terminal_0
   Skipping - not zephyr-philosophers
   Skipping - not zephyr-synchronization
   RESULTS:
   RESULTS - zephyr_v8r64.ZephyrTests.test_helloworld: PASSED (0.73s)
   RESULTS - zephyr_v8r64.ZephyrTests.test_philosophers: SKIPPED (0.00s)
   RESULTS - zephyr_v8r64.ZephyrTests.test_synchronization: SKIPPED (0.00s)
   SUMMARY:
   zephyr-helloworld () - Ran 3 tests in 0.731s
   zephyr-helloworld - OK - All required tests passed (successes=1, skipped=2, failures=0, errors=0)

The logs can be viewed at (using ``zephyr-synchronization`` as an example):

* Testimage output: ``build/tmp_baremetal-zephyr/work/armv8r-poky-linux/zephyr-synchronization/3.2.0+gitAUTOINC+4256cd41df-r0/temp/log.do_testimage``
* Console output: ``build/tmp_baremetal-zephyr/work/armv8r-poky-linux/zephyr-synchronization/3.2.0+gitAUTOINC+4256cd41df-r0/testimage/default_log``

Baremetal Linux
===============

For the Linux OS, to build the image with the test suite and run it, use the
following command:

.. code-block:: shell

   TESTIMAGE_AUTO=1 kas build v8r64/meta-armv8r64-extras/kas/baremetal-linux.yml

An example of the test suite output is as follows:

.. code-block:: shell

   Creating terminal default on terminal_0
   default: Waiting for login prompt
   RESULTS:
   RESULTS - linuxboot.LinuxBootTest.test_linux_boot: PASSED (89.56s)
   RESULTS - basictests.BasicTests.test_basic_tests: PASSED (155.99s)
   RESULTS - sysinfo.SysInfo.test_hostname: PASSED (3.41s)
   RESULTS - sysinfo.SysInfo.test_kernel_version: PASSED (3.34s)
   SUMMARY:
   core-image-minimal () - Ran 4 tests in 252.298s
   core-image-minimal - OK - All required tests passed (successes=4, skipped=0, failures=0, errors=0)

The logs can be viewed at:

 * Testimage output: ``build/tmp_baremetal-linux/work/fvp_baser_aemv8r64-poky-linux/core-image-minimal/1.0-r0/temp/log.do_testimage``
 * Console output: ``build/tmp_baremetal-linux/work/fvp_baser_aemv8r64-poky-linux/core-image-minimal/1.0-r0/testimage/default_log``

Virtualization
==============

For the Virtualization stack, to build the image with the test suite and run
it, use the following command:

.. code-block:: shell

   XEN_DOM0LESS_DOM_LINUX_DEMO_AUTORUN=0 \
   TESTIMAGE_AUTO=1 \
       kas build v8r64/meta-armv8r64-extras/kas/virtualization.yml

This runs the same test cases as for the baremetal Linux and Zephyr stacks, as
well as an additional test case against the Xen console output.

.. note::

   When setting ``TESTIMAGE_AUTO=1`` for automatic testing,
   ``XEN_DOM0LESS_DOM_LINUX_DEMO_AUTORUN`` must be set to 0, and vice
   versa. That is, ``TESTIMAGE_AUTO`` and
   ``XEN_DOM0LESS_DOM_LINUX_DEMO_AUTORUN`` cannot be set to ``1`` at the same
   time, because demo application autorun and ``testimage`` both start the
   ``rpmsg-remote`` application, which will cause ``testimage`` to fail.

An example of the test suite output is as follows:

.. code-block:: shell

   Creating terminal default on terminal_2
   Creating terminal xen on terminal_0
   Creating terminal zephyr_v8r64 on terminal_1
   default: Waiting for login prompt
   ptest-runner started
   Skipping - not zephyr-helloworld
   Skipping - not zephyr-philosophers
   Skipping - not zephyr-synchronization
   RESULTS:
   RESULTS - linuxboot.LinuxBootTest.test_linux_boot: PASSED (145.80s)
   RESULTS - basictests.BasicTests.test_basic_tests: PASSED (181.35s)
   RESULTS - linuxlogin.LinuxLoginTest.test_linux_login: PASSED (4.58s)
   RESULTS - ptest_docker.PtestDockerTests.test_ptestdocker: PASSED (439.05s)
   RESULTS - rpmsg.RpmsgTests.test_rpmsg_demo: PASSED (62.72s)
   RESULTS - rpmsg.RpmsgTests.test_zephyr_boot: PASSED (0.00s)
   RESULTS - sysinfo.SysInfo.test_hostname: PASSED (5.30s)
   RESULTS - sysinfo.SysInfo.test_kernel_version: PASSED (3.28s)
   RESULTS - xen.XenBoot.test_boot: PASSED (0.00s)
   RESULTS - zephyr_v8r64.ZephyrTests.test_helloworld: SKIPPED (0.00s)
   RESULTS - zephyr_v8r64.ZephyrTests.test_philosophers: SKIPPED (0.00s)
   RESULTS - zephyr_v8r64.ZephyrTests.test_synchronization: SKIPPED (0.00s)
   SUMMARY:
   core-image-minimal () - Ran 12 tests in 842.089s
   core-image-minimal - OK - All required tests passed (successes=9, skipped=3, failures=0, errors=0)

The logs can be viewed at:

 * Testimage output: ``build/tmp_virtualization/work/fvp_baser_aemv8r64-poky-linux/core-image-minimal/1.0-r0/temp/log.do_testimage``
 * Xen console output: ``build/tmp_virtualization/work/fvp_baser_aemv8r64-poky-linux/core-image-minimal/1.0-r0/testimage/xen_log``
 * Zephyr console output: ``build/tmp_virtualization/work/fvp_baser_aemv8r64-poky-linux/core-image-minimal/1.0-r0/testimage/zephyr_v8r64_log``
 * Linux console output: ``build/tmp_virtualization/work/fvp_baser_aemv8r64-poky-linux/core-image-minimal/1.0-r0/testimage/default_log``

*********
Reference
*********

.. [1] https://developer.arm.com/downloads/-/arm-ecosystem-fvps/eula
.. [2] https://docs.yoctoproject.org/test-manual/intro.html
