..
 # Copyright (c) 2022, Arm Limited.
 #
 # SPDX-License-Identifier: MIT

.. _User Guide:

##########
User Guide
##########

.. toctree::
   :maxdepth: 1
   :caption: Contents

   reproduce

..

   Instructions for setting up the build environment, checking out code,
   building, running and validating the key use cases.

.. toctree::
   :maxdepth: 1

   extend

..

   Guides showing how to use extra features, customize configurations in this
   Software Stack.

.. toctree::
   :maxdepth: 1

   borrow

..

   A deeper explanation of how to reuse the components, patches in this Software
   Stack.

