..
 # Copyright (c) 2022, Arm Limited.
 #
 # SPDX-License-Identifier: MIT

######
Borrow
######

This document is a deeper explanation of how to reuse the components, patches
in this Software Stack.

****************************
Reusing the Firmware Patches
****************************

The firmware (``linux-system.axf``) consists of U-Boot and the base device tree,
bundled together with boot-wrapper-aarch64. Both U-Boot and boot-wrapper-aarch64
are patched to support the Armv8-R AArch64 architecture. These patches live in
``meta-arm-bsp`` [1]_.

For further details on the boot-wrapper-aarch64 patches see the
:ref:`Boot-wrapper-aarch64` section in :doc:`/manual/index`. The U-Boot
:ref:`Additional Patches` section provides more details on the U-Boot patches.

The base device tree [2]_ can be found in the meta-arm-bsp Yocto layer.

***********************
Reusing the Xen Patches
***********************

The patch series for Armv8-R AArch64 with MPU support, which are located in
:repo:`meta-armv8r64-extras/dynamic-layers/virtualization-layer/recipes-extended/xen/files`,
initialize the PoC (Proof of Concept) of Xen on the Armv8-R AArch64
architecture. These patches are implemented based on Xen `4.17`_, and the work
to upstream these patches to the Xen mainline is in progress.

.. _4.17: https://xenbits.xen.org/gitweb/?p=xen.git;a=tree;h=refs/heads/stable-4.17

*********
Reference
*********

.. [1] https://git.yoctoproject.org/meta-arm/tree/meta-arm-bsp?h=langdale

.. [2] https://git.yoctoproject.org/meta-arm/tree/meta-arm-bsp/recipes-kernel/linux/files/fvp-baser-aemv8r64/fvp-baser-aemv8r64.dts?h=langdale
