..
 # Copyright (c) 2022-2023, Arm Limited.
 #
 # SPDX-License-Identifier: MIT

###################################################
Armv8-R AArch64 Extras Layer (meta-armv8r64-extras)
###################################################

.. _Zephyr Sample Applications:

**************************
Zephyr Sample Applications
**************************

This Software Stack supports four Zephyr sample applications:

  * zephyr-helloworld
  * zephyr-synchronization
  * zephyr-philosophers
  * zephyr-rpmsg-demo (Zephyr part of :ref:`RPMsg Demo`)

Recipes for ``zephyr-helloworld``, ``zephyr-synchronization`` and
``zephyr-philosophers`` are provided by ``meta-zephyr``. Source code for these
three sample applications are provided by Zephyr, with extra configuration
provided for ``zephyr-philosophers`` by file
:repo:`meta-armv8r64-extras/dynamic-layers/zephyrcore/recipes-kernel/zephyr-kernel/zephyr-philosophers.bbappend`.
These three applications can work in both the baremetal stack and the
Virtualization stack.

The zephyr-rpmsg-demo application works in the Virtualization stack only. It
comes from the recipe
:repo:`meta-armv8r64-extras/dynamic-layers/virtualization-layer/recipes-kernel/zephyr-kernel/zephyr-rpmsg-demo.bb`.
This application works as a RPMsg host (``rpmsg-host``) in the Zephyr domain,
and co-works with the RPMsg remote application (``rpmsg-remote``) running in the
Linux domain to implement the Inter-VM communication through the RPMsg
protocol. Recipe for the ``rpmsg-remote`` application is provided by
:repo:`meta-armv8r64-extras/dynamic-layers/virtualization-layer/recipes-demo/rpmsg-demo/rpmsg-demo.bb`,
which along with ``zephyr-rpmsg-demo.bb`` is provided by this Software Stack.
The source code is also provided by this Software Stack and can be found in
directory :repo:`components/apps/rpmsg-demo`. For more details, see
:ref:`RPMsg Demo` under :ref:`Demo Applications`.

Running Zephyr applications is supported by the ``runfvp`` script from
meta-arm. Section :ref:`Build and Run` has examples that uses this script.

********************
Virtualization Stack
********************

The Virtualization stack uses Xen as the Type-1 hypervisor to boot one or more
domains independently. The meta-virtualization layer is used to provide Xen
build support.

The Xen MPU implementation only supports "dom0less" mode (for more details see
the :ref:`Hypervisor (Xen)` section), so the meta-armv8r64-extras layer provides
logic to build the required dom0less artifacts, which are shown in the diagram
below. For more information on how U-Boot detects and uses these artifacts at
runtime, see the U-Boot :ref:`Boot Sequence`.

.. image:: /images/armv8r64-xen-stack.png
   :alt: Arm v8r64 Xen stack

Domain Configuration
====================

The default domain configuration is provided in a bbclass. For more information,
see :ref:`Customizing the Xen Domains`.

Xen Configuration
=================

Xen is configured through the ``.bb`` and ``.bbappend`` files in directory
:repo:`meta-armv8r64-extras/dynamic-layers/virtualization-layer/recipes-extended/xen`,
which define the branch and revision to build for the ``fvp-baser-aemv8r64``
machine and also includes a machine-specific config file.

Linux Image Configuration
=========================

In projects based on OE-core, the Linux image recipe is responsible for creating
the disk image, including the root filesystem partition. The boot partition,
which is part of the same disk image, is populated by extending
``IMAGE_CLASSES`` (see `Image Generation`_ below).

No changes are made to the Linux kernel for the Virtualization use case.

Zephyr Application Configuration
================================

Zephyr requires some specific configuration when running as a Xen domain on the
``fvp-baser-aemv8r64`` machine. It is necessary to match the number of CPUs
allocated, the DRAM address and the selected UART with the equivalent parameters
in the domain configuration. This is achieved using Xen-specific overlay files
(see :ref:`Customizing the Zephyr Configuration`).

Device Tree Generation
======================

Xen domains in dom0less mode are configured using additions to the ``/chosen``
node in the device tree. To avoid modifying the firmware's device tree for the
Virtualization stack we instead dynamically generate a device tree overlay file
which can be applied at runtime by U-Boot. Additionally, in order for Xen to
access peripherals in dom0less mode, we must specify which peripherals to "pass
through" to each domain using a domain-specific passthrough dtb file (see
https://xenbits.xen.org/docs/unstable/misc/arm/passthrough.txt for more
details).

All these device trees are created from templates, substituting placeholders
with values defined in the domain configuration. The passthrough device tree
template to use for each domain is selected in the domain configuration.

U-Boot Script Generation
========================

The boot script is partially dynamically generated in order to load the
configured domain binaries and passthrough device trees to the configured memory
addresses prior to booting Xen.

Image Generation
================

The boot partition needs to be populated with the artifacts necessary to boot
Xen and its domains so they can be loaded by U-Boot. This is achieved using a
custom Wic image and a custom bbclass (``xen_image_dom0less``). This class can
be added to ``IMAGE_CLASSES`` so that the desired Linux image recipe
(e.g. ``core-image-minimal``) includes the necessary configuration to populate
the boot partition.
