..
 # Copyright (c) 2022, Arm Limited.
 #
 # SPDX-License-Identifier: MIT

############
Boot Process
############

As described in the :ref:`High Level Architecture` section of the :doc:`/intro`,
the system can boot in 3 different ways. The corresponding boot flow is as
follows:

The booting process of Baremetal Zephyr is quite straightforward: Zephyr boots
directly from the reset vector after system reset.

For Baremetal Linux, the booting process is as the following diagram:

.. image:: /images/armv8r64-baremetal-linux-boot.png
   :alt: Armv8-R Stack Boot Flow (Baremetal Linux)

And the booting process of Virtualization solution is as the following diagram:

.. image:: /images/armv8r64-virtualization-boot.png
   :alt: Armv8-R Stack Boot flow (Virtualization - Xen Hypervisor)

The :ref:`Boot Sequence` section provides more details.
