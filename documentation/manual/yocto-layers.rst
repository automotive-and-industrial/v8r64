..
 # Copyright (c) 2022-2023, Arm Limited.
 #
 # SPDX-License-Identifier: MIT

############
Yocto Layers
############

The Armv8-R AArch64 Software Stack is provided through the GitLab repository:
|v8r64 repository|. The v8r64 repository provides a Yocto Project compatible
layer -- ``meta-armv8r64-extras`` -- with target platform `fvp-baser-aemv8r64`_
extensions for Armv8-R AArch64. Currently this layer extends the
``fvp-baser-aemv8r64`` machine definition from the meta-arm-bsp layer in the
`meta-arm`_ repository.

.. _fvp-baser-aemv8r64: https://git.yoctoproject.org/meta-arm/tree/meta-arm-bsp/documentation/fvp-baser-aemv8r64.md
.. _meta-arm: https://git.yoctoproject.org/meta-arm/tree/?h=langdale

The following diagram illustrates the layers which are integrated in the
Software Stack, which are further expanded below.

.. image:: /images/armv8r64-yocto-layers.png
   :alt: Arm v8r64 Yocto layers

* poky layers (meta, meta-poky)

  * URL: https://git.yoctoproject.org/poky/?h=langdale
  * Provides the base configuration, including the 'poky' distro.

* meta-arm layers (meta-arm-bsp, meta-arm, meta-arm-toolchain)

  * URL: https://git.yoctoproject.org/meta-arm/?h=langdale
  * meta-arm-bsp contains the board support to boot baremetal Linux for the
    ``fvp-baser-aemv8r64`` machine. It includes

    * The base device tree
    * Machine-specific boot-wrapper-aarch64 patches
    * Machine-specific U-Boot patches

  * meta-arm contains:

    * A recipe for the FVP_Base_AEMv8R model itself
    * The base recipe for boot-wrapper-aarch64

  * meta-arm-toolchain is not used, but is a dependency of meta-arm.

* meta-virtualization

  * URL: https://git.yoctoproject.org/meta-virtualization/?h=langdale
  * Included for the Virtualization stack only.

* meta-openembedded layers (meta-oe, meta-python, meta-filesystems,
  meta-networking)

  * URL: http://git.openembedded.org/meta-openembedded?h=langdale
  * These are dependencies of meta-virtualization and meta-zephyr. ``meta-oe``
    and ``meta-python`` are included for all configurations.
    ``meta-filesystems`` and ``meta-networking`` are included for the
    Virtualization stack only.

* meta-zephyr layer (meta-zephyr-core)

  * URL: https://git.yoctoproject.org/meta-zephyr/?h=langdale
  * Provides Zephyr SDK bundle, which is used to build Zephyr applications.
  * Provides recipes for ``zephyr-helloworld``, ``zephyr-synchronization`` and
    ``zephyr-philosophers``.

* meta-openamp

  * URL: https://github.com/OpenAMP/meta-openamp
  * Included for the Virtualization stack only.
  * Provides support for building libmetal and libopen_amp libraries.
