..
 # Copyright (c) 2022, Arm Limited.
 #
 # SPDX-License-Identifier: MIT

###################
Codeline Management
###################

********
Overview
********

The Armv8-R AArch64 Software Stack releases use the following model:

  * The Software Stack releases grow incrementally (meaning that we are not
    dropping previously released features, with exceptions)

    .. note::

       A specific exception is that support for PREEMPT_RT Linux builds was
       removed in :ref:`Release 4.0 - Xen`.

  * The `Armv8-R AEM FVP model`_ releases are incremental (meaning that FVPs are
    not dropping previously released features)

.. _Armv8-R AEM FVP model: https://developer.arm.com/downloads/-/arm-ecosystem-models

In addition to the above model, the Armv8-R AArch64 Software Stack is developed
and released based on Yocto's release branch process. This strategy allows us
make releases based on upstream stable branches, reducing the risk of having
build and runtime issues.

*********************
Yocto Release Process
*********************

.. image:: /images/yocto-release-process.png
   :alt: Yocto Release Process
   :align: center

The diagram above gives an overview of the Yocto branch and release process:

  * |inclusivity-exception| Development happens primarily in the ``master``
    (or ``main``) branch.
  * The project has a major release roughly every 6 months where a stable branch
    is created.
  * Each major release has a *codename* which is also used to name the stable
    branch is created.
  * Once a stable branch is created and released, it only receives bug fixes
    with minor (point) releases on an unscheduled basis.
  * The goal is for users and 3rd parties layers to use these *codename*
    branches as a means to be compatible with each other.

For a complete description of the Yocto release process, support schedule and
other details, see the `Yocto Release Process`_ documentation.

.. _Yocto Release Process:
   https://docs.yoctoproject.org/ref-manual/release-process.html

*********************************************************
Armv8-R AArch64 Software Stack Branch and Release Process
*********************************************************

The Armv8-R AArch64 Software Stack's branch and release process can be described
as the following diagram:

.. image:: /images/armv8r64-software-stack-release-process.png
   :alt: Armv8-R AArch64 Software Stack Release Process
   :align: center

The stack's branch and release process will follow the Yocto release process.
Below is a detailed description of the branch strategy for this stack's
development and release process.

  * Main branch

    * Represented by the green line on the diagram above.
    * The repository's ``main`` branch is meant to be compatible with
      |inclusivity-exception| ``master`` or ``main`` branches from Poky and
      3rd party layers.
    * This stack is not actively developed on this ``main`` branch to avoid the
      instability inherited from Yocto development on the
      |inclusivity-exception| ``master`` branch.
    * To reduce the effort to move this stack to a new version of Yocto, this
      ``main`` branch is periodically updated with the patches from the
      development and release branch on a regular basis.

  * Development and release branches

    * Represented by the blue line on the diagram above.
    * This stack uses development branches based on or compatible with Yocto
      stable branches.
    * A development branch in this stack is setup for each new Yocto release
      using the name convention *codename* where *codename* comes from target
      Yocto release.
    * The development branches are where fixes, improvements and new features
      are developed.
    * On a regular basis, code from the development branch is ported to the
      ``main`` branch to reduce the effort required to move this stack to a new
      version of Yocto.
    * The development branches are also used for release by creating tags on the
      specific commits.
