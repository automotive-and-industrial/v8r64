# Copyright (c) 2014, Mentor Graphics Corporation. All rights reserved.
# Copyright (c) 2015 Xilinx, Inc. All rights reserved.
# Copyright (c) 2016 Xilinx, Inc. All rights reserved.
# Copyright (c) 2016 Freescale Semiconductor, Inc. All rights reserved
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: BSD-3-Clause

if (WITH_LIBMETAL_FIND)
  find_package (Libmetal REQUIRED)
  collect (PROJECT_INC_DIRS "${LIBMETAL_INCLUDE_DIR}")
  collect (PROJECT_LIB_DIRS "${LIBMETAL_LIB_DIR}")
  collect (PROJECT_LIB_DEPS "${LIBMETAL_LIB}")
endif (WITH_LIBMETAL_FIND)

if (WITH_OPENAMP_FIND)
  find_package (OpenAmp REQUIRED)
  collect (PROJECT_INC_DIRS "${OPENAMP_INCLUDE_DIR}")
  collect (PROJECT_LIB_DIRS "${OPENAMP_LIB_DIR}")
  collect (PROJECT_LIB_DEPS "${OPENAMP_LIB}")
endif (WITH_OPENAMP_FIND)

if ("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux")
  check_include_files (stdatomic.h HAVE_STDATOMIC_H)
  check_include_files (fcntl.h HAVE_FCNTL_H)

  find_package(Threads REQUIRED)
  collect (PROJECT_LIB_DEPS "${CMAKE_THREAD_LIBS_INIT}")
else ("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux")
  set (_saved_cmake_required_flags ${CMAKE_REQUIRED_FLAGS})
  set (CMAKE_REQUIRED_FLAGS "-c")
  check_include_files (stdatomic.h HAVE_STDATOMIC_H)
  check_include_files (fcntl.h HAVE_FCNTL_H)
  set (CMAKE_REQUIRED_FLAGS ${_saved_cmake_required_flags})
endif ("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux")
