# Copyright (c) 2014, Mentor Graphics Corporation. All rights reserved.
# Copyright (c) 2015 Xilinx, Inc. All rights reserved.
# Copyright (c) 2016 Xilinx, Inc. All rights reserved.
# Copyright (c) 2016 Freescale Semiconductor, Inc. All rights reserved
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: BSD-3-Clause

set (CMAKE_SYSTEM_NAME      "Generic" CACHE STRING "")

include (CMakeForceCompiler)

CMAKE_FORCE_C_COMPILER   ("${CROSS_PREFIX}gcc" GNU)
CMAKE_FORCE_CXX_COMPILER ("${CROSS_PREFIX}g++" GNU)

set (CMAKE_FIND_ROOT_PATH_MODE_PROGRAM	NEVER CACHE STRING "")
set (CMAKE_FIND_ROOT_PATH_MODE_LIBRARY	NEVER CACHE STRING "")
set (CMAKE_FIND_ROOT_PATH_MODE_INCLUDE	NEVER CACHE STRING "")
