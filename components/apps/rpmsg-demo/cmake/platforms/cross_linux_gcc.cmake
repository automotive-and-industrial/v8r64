# Copyright (c) 2014, Mentor Graphics Corporation. All rights reserved.
# Copyright (c) 2015 Xilinx, Inc. All rights reserved.
# Copyright (c) 2016 Xilinx, Inc. All rights reserved.
# Copyright (c) 2016 Freescale Semiconductor, Inc. All rights reserved
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: BSD-3-Clause

set (CMAKE_SYSTEM_NAME      "Linux")
set (CMAKE_C_COMPILER       "${CROSS_PREFIX}gcc")
set (CMAKE_CXX_COMPILER     "${CROSS_PREFIX}g++")

set (CMAKE_FIND_ROOT_PATH_MODE_PROGRAM	NEVER)
set (CMAKE_FIND_ROOT_PATH_MODE_LIBRARY	NEVER)
set (CMAKE_FIND_ROOT_PATH_MODE_INCLUDE	NEVER)
