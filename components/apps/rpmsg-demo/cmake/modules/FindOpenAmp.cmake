# Copyright (c) 2014, Mentor Graphics Corporation. All rights reserved.
# Copyright (c) 2015 Xilinx, Inc. All rights reserved.
# Copyright (c) 2016 Xilinx, Inc. All rights reserved.
# Copyright (c) 2016 Freescale Semiconductor, Inc. All rights reserved
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: BSD-3-Clause

# FindOpenAmp
# --------
#
# Find open-amp
#
# Find the native open-amp includes and library this module defines
#
# ::
#
#   OPENAMP_INCLUDE_DIR, where to find openamp/open_amp.h, etc.
#   LIBSYSFS_LIB_DIR, where to find openamp library.

find_path(OPENAMP_INCLUDE_DIR NAMES openamp/open_amp.h PATHS ${CMAKE_FIND_ROOT_PATH})
find_library(OPENAMP_LIB NAMES open_amp PATHS ${CMAKE_FIND_ROOT_PATH})
get_filename_component(OPENAMP_LIB_DIR ${OPENAMP_LIB} DIRECTORY)

include (FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS (OpenAmp DEFAULT_MSG OPENAMP_LIB OPENAMP_INCLUDE_DIR)

if (OPENAMP_FOUND)
  set (OPENAMP_LIBS ${OPENAMP_LIB})
endif (OPENAMP_FOUND)

mark_as_advanced (OPENAMP_LIB OPENAMP_INCLUDE_DIR OPENAMP_LIB_DIR)
