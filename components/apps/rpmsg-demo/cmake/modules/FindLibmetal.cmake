# Copyright (c) 2014, Mentor Graphics Corporation. All rights reserved.
# Copyright (c) 2015 Xilinx, Inc. All rights reserved.
# Copyright (c) 2016 Xilinx, Inc. All rights reserved.
# Copyright (c) 2016 Freescale Semiconductor, Inc. All rights reserved
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: BSD-3-Clause

# FindLibmetal
# --------
#
# Find Libmetal
#
# Find the native Libmetal includes and library this module defines
#
# ::
#
#   LIBMETAL_INCLUDE_DIR, where to find metal/sysfs.h, etc.
#   LIBSYSFS_LIB_DIR, where to find libmetal library.

find_path(LIBMETAL_INCLUDE_DIR NAMES metal/sys.h PATHS ${CMAKE_FIND_ROOT_PATH})
find_library(LIBMETAL_LIB NAMES metal PATHS ${CMAKE_FIND_ROOT_PATH})
get_filename_component(LIBMETAL_LIB_DIR ${LIBMETAL_LIB} DIRECTORY)

include (FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS (Libmetal DEFAULT_MSG LIBMETAL_LIB LIBMETAL_INCLUDE_DIR)

if (LIBMETAL_FOUND)
  set (LIBMETAL_LIBS ${LIBMETAL_LIB})
endif (LIBMETAL_FOUND)

mark_as_advanced (LIBMETAL_LIB LIBMETAL_INCLUDE_DIR LIBMETAL_LIB_DIR)
