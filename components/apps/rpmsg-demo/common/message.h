/*
 * Copyright (c) 2022, Arm Limited.
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __MESSAGE_H__
#define __MESSAGE_H__

#define DEMO1_MSG_LIMIT     100U
#define DEMO2_MSG_LIMIT     10U
#define MAGIC               0x49564D43;      /* "IVMC" */

struct payload {
    uint32_t magic;
    uint32_t id;
    uint64_t timestamp;
    uint16_t type;
    uint8_t  reserved[6];
    union {
        uint8_t  bytes[32];
        uint16_t words[16];
        uint32_t dwords[8];
        uint64_t qwords[4];
    } data;
    uint16_t checksum;
    uint8_t padding[2];
};

#endif /* __MESSAGE_H__ */
