/*
 * Copyright (c) 2022, Arm Limited.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __CONFIG_H__
#define __CONFIG_H__

#define SHMEM_BASE_ADDRESS      0x70000000
#define SHMEM_REGION_SIZE       0x01000000

#define VDEV_START_ADDR         SHMEM_BASE_ADDRESS
#define VDEV_SIZE               SHMEM_REGION_SIZE

#define VDEV_STATUS_ADDR        VDEV_START_ADDR
#define VDEV_STATUS_SIZE        0x400

#define SHM_START_ADDR          (VDEV_START_ADDR + VDEV_STATUS_SIZE)
#define SHM_SIZE                (VDEV_SIZE - VDEV_STATUS_SIZE)
#define SHM_DEVICE_NAME         "/dev/xen_mem"

#define VRING_COUNT             2
#define VRING_RX_ADDRESS        (VDEV_START_ADDR + SHM_SIZE - VDEV_STATUS_SIZE)
#define VRING_TX_ADDRESS        (VDEV_START_ADDR + SHM_SIZE)
#define VRING_ALIGNMENT         4
#define VRING_SIZE              16

#define EVTCHN_DEVICE_NAME      "/dev/xen/evtchn"
#define EVTCHN_PORT_DOM_ZEPHYR  0x0A
#define EVTCHN_PORT_DOM_LINUX   0x0B

#define IVM_METAL_BUS_NAME      "xen_ivm"

typedef uint32_t evtchn_port_t;

#endif /* __CONFIG_H__ */
