/*
 * Copyright (c) 2022, Arm Limited.
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __DEVICE_H__
#define __DEVICE_H__

#ifdef __TARGET_OS_LINUX__
#include <poll.h>
#endif

#include <openamp/open_amp.h>
#include <metal/alloc.h>
#include <metal/device.h>

#include "config.h"

#define IVM_DEV_NAME    "xen_ivm"

struct ivm_shmem_device {
    const char      *name;
    int             fd;
    size_t          size;
    unsigned long   base_phys_addr;
    unsigned long   base_virt_addr;
};

struct ivm_evtchn_device {
    const char      *name;
    int             fd;
    evtchn_port_t   port;
#ifdef __TARGET_OS_LINUX__
    struct pollfd   pfd;
#endif
};

/* ivm virtio device info */
struct ivm_vdev_info {
    unsigned long   start_addr;
    size_t          size;
    unsigned long   status_addr;
    size_t          status_size;
};

/* ivm shm pool info */
struct ivm_shm_pool_info {
    unsigned long   start_addr;
    size_t          size;
};

struct ivm_vring_info {
    unsigned long rx_addr;
    unsigned long tx_addr;
};

struct ivm_device {
    /* Device name */
    const char                  *name;
    /* Bus that contains device */
    struct metal_bus            *bus;

    /* shmem device */
    struct ivm_shmem_device     shmem_dev;

    /* evtchn device */
    struct ivm_evtchn_device    evtchn_dev;

    /* virtio device */
    struct ivm_vdev_info        vdev_info;

    /* shm pool */
    struct ivm_shm_pool_info    shm_pool_info;

    /* vring */
    struct ivm_vring_info       vring_info;

    /* private data */
    void *priv;
};

int ivm_device_init(void *data);

int ivm_device_open(struct metal_bus *bus, const char *dev_name,
                    struct metal_device **device);
void ivm_device_close(struct metal_bus *bus, struct metal_device *device);

int ivm_device_notify(void);
int ivm_device_wait_event(void);

struct ivm_shmem_device *ivm_device_get_shmem_dev(void);
struct ivm_evtchn_device *ivm_device_get_evtchn_dev(void);
struct ivm_vdev_info *ivm_device_get_vdev_info(void);
struct ivm_shm_pool_info *ivm_device_get_shm_pool_info(void);
struct ivm_vring_info *ivm_device_get_vring_info(void);

#endif /* __DEVICE_H__ */
