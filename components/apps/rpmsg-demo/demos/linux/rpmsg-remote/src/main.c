/*
 * Copyright (c) 2022, Arm Limited.
 * SPDX-License-Identifier: BSD-3-Clause
 */

/*
 * This is a sample demonstration application that showcases usage of rpmsg.
 * This application is meant to run in the Linux domain of the Xen hypervisor.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>
#include <poll.h>
#include <assert.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdatomic.h>

#include <metal/alloc.h>
#include <metal/device.h>
#include <metal/list.h>
#include <metal/irq.h>
#include <openamp/open_amp.h>

#include "config.h"
#include "shmem.h"
#include "evtchn.h"
#include "device.h"
#include "message.h"

#define RPMSG_NUM_VRINGS            2
#define RPMSG_VRING_IDX_TX          0
#define RPMSG_VRING_IDX_RX          1

extern int metal_linux_irq_init(void);

static metal_phys_addr_t shm_physmap[] = { SHM_START_ADDR };
static struct metal_device shm_device = {
    .name = SHM_DEVICE_NAME,
    .bus = NULL,
    .num_regions = 1,
    .regions = {
        [0] = {
            .virt       = (void *)(-1), /* .virt will be set to proper address
                                         * later when open device, e,g. in
                                         * bus->ops.dev_open()
                                         */
            .physmap    = shm_physmap,
            .size       = SHM_SIZE,
            .page_shift = (metal_phys_addr_t)(-1),
            .page_mask  = (metal_phys_addr_t)(-1),
            .mem_flags  = 0,
            .ops        = { NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL },
        },
    },
    .node = { NULL },
    .irq_num = 1,
    .irq_info = NULL,
};

static sem_t data_rx_sem;
static uint8_t rpmsg_recv_buf[RPMSG_BUFFER_SIZE];
static uint8_t rpmsg_send_buf[RPMSG_BUFFER_SIZE];
static bool rpmsg_test_mode = false;

static struct virtio_vring_info rvrings[RPMSG_NUM_VRINGS] = {
    [0] = {
        .info.align = VRING_ALIGNMENT,
    },
    [1] = {
        .info.align = VRING_ALIGNMENT,
    },
};
static struct virtio_device vdev;
static struct rpmsg_virtio_device rvdev;
static struct metal_io_region *io;
static struct virtqueue *vq[RPMSG_NUM_VRINGS];

static struct rpmsg_endpoint rept;
static struct rpmsg_endpoint *ep = &rept;

static uint8_t virtio_get_status(struct virtio_device *vdev)
{
    (void)vdev;

    struct ivm_vdev_info *pvdev_info = ivm_device_get_vdev_info();
    unsigned long addr = pvdev_info->status_addr;

    return *(uint8_t *)addr;
}

static void virtio_set_status(struct virtio_device *vdev, uint8_t status)
{
    (void)vdev;

    struct ivm_vdev_info *pvdev_info = ivm_device_get_vdev_info();
    unsigned long addr = pvdev_info->status_addr;

    memcpy((void *)addr, &status, sizeof(status));
}

static uint32_t virtio_get_features(struct virtio_device *vdev)
{
    (void)vdev;

    return (1 << VIRTIO_RPMSG_F_NS);
}

static void virtio_set_features(struct virtio_device *vdev, uint32_t features)
{
    (void)vdev;
    (void)features;
}

static void virtio_notify(struct virtqueue *vq)
{
    (void)vq;

    ivm_device_notify();
}

struct virtio_dispatch dispatch = {
    .get_status   = virtio_get_status,
    .set_status   = virtio_set_status,
    .get_features = virtio_get_features,
    .set_features = virtio_set_features,
    .notify       = virtio_notify,
};

int endpoint_cb(struct rpmsg_endpoint *ept, void *data,
                size_t len, uint32_t src, void *priv)
{
    (void)ept;
    (void)src;
    (void)priv;

    memset(rpmsg_recv_buf, 0, sizeof(rpmsg_recv_buf));
    memcpy(rpmsg_recv_buf, data, len);
    (void)sem_post(&data_rx_sem);

    return RPMSG_SUCCESS;
}

static void rpmsg_service_unbind(struct rpmsg_endpoint *ept)
{
    (void)ept;

    rpmsg_destroy_ept(ep);
}

static int wait_message()
{
    return sem_wait(&data_rx_sem);
}

static int send_message(uint8_t *data, size_t len)
{
    return rpmsg_send(ep, data, len);
}

int irq_handler(int irq, void *arg)
{
    (void)irq;
    (void)arg;

    struct ivm_evtchn_device *pevdev = ivm_device_get_evtchn_dev();

    evtchn_pending();

    virtqueue_notification(vq[RPMSG_VRING_IDX_RX]);

    evtchn_unmask(pevdev->port);

    return METAL_IRQ_HANDLED;
}

/*
 * Application entry point
 */
void *app(void *priv)
{
    int status = 0;
    unsigned int msg_idx = 0U;
    struct metal_device *device = NULL;
    struct rpmsg_device *rdev;
    struct metal_init_params metal_params = METAL_INIT_DEFAULTS;
    uint32_t msg_received_count = 0U;

    (void)priv;
    metal_params.log_level = METAL_LOG_DEBUG;

    printf("\r\nOpenAMP [RPMsg-Remote] demo started.\r\n");

    ivm_device_init(NULL);

    status = metal_init(&metal_params);
    /*
     * Known Issue:
     * The return status will be ENODEV (-19).
     * Currently we only metal_init() to initialize the common field
     * (_metal.common), metal_sys_init() is not used.
     * So ignore the failure and continue
     */
    (void)status;

    status = metal_bus_register(&metal_generic_bus);
    if (status != 0) {
        printf("Failed to register bus: %d\n", status);
        goto _out;
    }

    status = metal_linux_irq_init();
    if (status != 0) {
        printf("Failed to init IRQ: %d\n", status);
        goto _out;
    }

    status = metal_register_generic_device(&shm_device);
    if (status != 0) {
        printf("Failed to register shm device: %d\n", status);
        goto _out;
    }

    status = metal_device_open(IVM_METAL_BUS_NAME, SHM_DEVICE_NAME, &device);
    if (status < 0) {
        printf("Failed to open %s - %s: %d\n",
               IVM_METAL_BUS_NAME, SHM_DEVICE_NAME, status);
        goto _out;
    }

    io = metal_device_io_region(device, 0);
    if (io == NULL) {
        printf("Failed to get IO region\n");
        goto _out;
    }

    /* Setup vdev */
    vq[RPMSG_VRING_IDX_TX] = virtqueue_allocate(VRING_SIZE);
    if (vq[RPMSG_VRING_IDX_TX] == NULL) {
        printf("Failed to allocate virtqueue vq[RPMSG_VRING_IDX_TX]\n");
        goto _out;
    }
    vq[RPMSG_VRING_IDX_RX] = virtqueue_allocate(VRING_SIZE);
    if (vq[RPMSG_VRING_IDX_RX] == NULL) {
        printf("Failed to allocate virtqueue vq[RPMSG_VRING_IDX_RX]\n");
        goto _out;
    }

    struct ivm_vring_info *pvring = ivm_device_get_vring_info();
    vdev.role = RPMSG_REMOTE;
    vdev.vrings_num = VRING_COUNT;
    vdev.func = &dispatch;
    rvrings[RPMSG_VRING_IDX_TX].io = io;
    rvrings[RPMSG_VRING_IDX_TX].info.vaddr = (void *)(pvring->tx_addr);
    rvrings[RPMSG_VRING_IDX_TX].info.num_descs = VRING_SIZE;
    rvrings[RPMSG_VRING_IDX_TX].info.align = VRING_ALIGNMENT;
    rvrings[RPMSG_VRING_IDX_TX].vq = vq[RPMSG_VRING_IDX_TX];

    rvrings[RPMSG_VRING_IDX_RX].io = io;
    rvrings[RPMSG_VRING_IDX_RX].info.vaddr = (void *)(pvring->rx_addr);
    rvrings[RPMSG_VRING_IDX_RX].info.num_descs = VRING_SIZE;
    rvrings[RPMSG_VRING_IDX_RX].info.align = VRING_ALIGNMENT;
    rvrings[RPMSG_VRING_IDX_RX].vq = vq[RPMSG_VRING_IDX_RX];

    vdev.vrings_info = &rvrings[0];

    struct ivm_evtchn_device *pevdev = ivm_device_get_evtchn_dev();
    status = metal_irq_register(pevdev->fd, irq_handler, NULL);
    if (status < 0) {
        printf("Failed to register irq: %d\n", status);
        goto _out;
    }

    /* Setup rvdev */
    status = rpmsg_init_vdev(&rvdev, &vdev, NULL, io, NULL);
    if (status != 0) {
        printf("Failed to init vdev: %d\n.", status);
        goto _out;
    }

    status = sem_init(&data_rx_sem, 0, 0);
    if (status != 0) {
        printf("Failed to init receive semaphore.\n");
        goto _out;
    }

    rdev = rpmsg_virtio_get_rpmsg_device(&rvdev);
    status = rpmsg_create_ept(ep, rdev, "k",
                              RPMSG_ADDR_ANY, RPMSG_ADDR_ANY,
                              endpoint_cb,
                              rpmsg_service_unbind);
    if (status != 0) {
        printf("Failed to create ept: %d\n", status);
        goto _out;
    }
    metal_irq_enable(pevdev->fd);

    /*
     * Demo #1
     * Receive messages from host and echo messages as reply
     */
    while (msg_idx < DEMO1_MSG_LIMIT) {
        status = wait_message();
        if (status < 0) {
            printf("Failed to lock the semaphore.\n");
            goto _out;
        }
        printf("Received message: %s\n", (char *)rpmsg_recv_buf);

        snprintf((char *)rpmsg_send_buf, sizeof(rpmsg_send_buf),
                 "echo - %02u", msg_idx);
        status = send_message(rpmsg_send_buf, strlen((char *)rpmsg_send_buf));
        if (status < 0) {
            printf("Failed to send_message (%u): %d\n", msg_idx, status);
            goto _out;
        }
        msg_idx++;
    }

    /*
     * Demo #2
     * Receive data from host and store data to file, and then
     * Nginx server running in a docker container expose the data to external
     */
    while (1) {
        int ret = -1;
        struct payload pd;
        char cmd[RPMSG_BUFFER_SIZE + 128];

        memset(&pd, 0, sizeof(pd));
        memset(cmd, 0, sizeof(cmd));
        status = wait_message();
        if (status < 0) {
            printf("Failed to lock the semaphore.\n");
            goto _out;
        }
        memcpy(&pd, rpmsg_recv_buf, sizeof(pd));
        /* Decode the payload and consume it */
        printf("Recv payload: idx - %u, uptime - %u, cycles - %u, "
               "thread: total - %lu, peak - %lu, avg - %lu\n",
               pd.id, pd.data.dwords[0], pd.data.dwords[1],
               pd.data.qwords[1], pd.data.qwords[2], pd.data.qwords[3]);

        /* Write the received data to file */
        /*
         * Insert a row in the table in HTML
         * Table scheme:
         * +-------+--------+----------+---------------------+------+-----+
         * | Index | Uptime | HW Clock | Thread Total Cycles | Peak | Avg |
         * +-------+--------+----------+---------------------+------+-----+
         * |       |        |          |                     |      |     |
         * +-------+--------+----------+---------------------+------+-----+
         */
        snprintf(cmd, sizeof(cmd),
                 "sed -i \"35a <p>"
                 "<tr><td>%u</td><td>%u</td><td>%u</td>"
                 "<td>%lu</td><td>%lu</td><td>%lu</td>"
                 "</tr>\" /usr/share/nginx/html/zephyr-status.html",
                 pd.id, pd.data.dwords[0], pd.data.dwords[1],
                 pd.data.qwords[1], pd.data.qwords[2], pd.data.qwords[3]);
        ret = system(cmd);
        /* Assume the command always executes successfully */
        (void)ret;

        /* Simply delete the oldest item if the total lines are more than 100 */
        snprintf(cmd, sizeof(cmd),
                 "fn='/usr/share/nginx/html/zephyr-status.html'\n"
                 "lines=$(wc -l $fn | awk '{print $1}')\n"
                 "if [[ $lines -ge 138 ]] ; then\n"
                 "    sed -i '136d' $fn\n"
                 "fi");
        ret = system(cmd);
        /* Assume the command always executes successfully */
        (void)ret;

        /* Counting if it's in a test mode */
        if (rpmsg_test_mode &&
            ++msg_received_count >= DEMO2_MSG_LIMIT) {
            break;
        }
    }

_out:
    metal_finish();
    (void)sem_destroy(&data_rx_sem);

    rpmsg_deinit_vdev(&rvdev);
    virtqueue_free(vq[RPMSG_VRING_IDX_RX]);
    virtqueue_free(vq[RPMSG_VRING_IDX_TX]);

    metal_bus_unregister(&metal_generic_bus);
    metal_device_close(device);     /* Will call ivm_device_close() to close
                                     * shmem and evtchn device
                                     */
    printf("OpenAMP demo ended.\n");

    return NULL;
}

static void parse_opt(int argc, char **argv)
{
    int option_index = 0;
    int c;

    const int8_t *usage =
        "Welcome to rpmsg-remote application.\n"
        "Usage:\n"
        "    rpmsg-remote [options]\n"
        "Options:\n"
        "-h, --help         Print this usage\n"
        "-t, --test_mode    Test mode (The demo2 "
        "will not work as an infinite loop)\n";

    struct option long_options[] = {
        {"help", no_argument, 0, 'h'},
        {"test_mode", no_argument, 0, 't'},
        {0, 0, 0, 0}
    };

    while (-1 != (c = getopt_long(argc, argv, "ht", long_options,
                                  &option_index))) {
        switch (c) {
            case 'h':
                printf("%s", usage);
                exit(0);
            case 't':
                rpmsg_test_mode = true;
                break;
        }
    }
}

int main(int argc, char *argv[])
{
    parse_opt(argc, argv);

    pthread_t thread1_id;

    printf("Starting application ...\r\n");

    pthread_create(&thread1_id, NULL, &app, NULL);
    pthread_join(thread1_id, NULL);

    printf("Stopping application ...\r\n");
    /* Cosmetic printf just to separate from debug message */
    printf("\n");

    return EXIT_SUCCESS;
}
