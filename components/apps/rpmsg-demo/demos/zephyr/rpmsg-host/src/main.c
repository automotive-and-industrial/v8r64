/*
 * Copyright (c) 2018, NXP
 * Copyright (c) 2018, Nordic Semiconductor ASA
 * Copyright (c) 2018, Linaro Limited
 * Copyright (c) 2019, Linaro Limited
 * Copyright (c) 2022, Arm Limited.
 * SPDX-License-Identifier: Apache-2.0
 */

/*
 * This is a sample demonstration application that showcases usage of rpmsg.
 * This application is meant to run in the Zephyr domain of the Xen hypervisor.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <zephyr/kernel.h>
#include <zephyr/sys/printk.h>
#include <zephyr/device.h>
#include <zephyr/init.h>
#include <zephyr/xen/events.h>

#include <openamp/open_amp.h>
#include <metal/device.h>

#include "config.h"
#include "device.h"
#include "message.h"

#define RPMSG_NUM_VRINGS            2
#define RPMSG_VRING_IDX_TX          0
#define RPMSG_VRING_IDX_RX          1

static unsigned long sample_idx = 0;

#define APP_TASK_STACK_SIZE (1024)
K_THREAD_STACK_DEFINE(thread_stack, APP_TASK_STACK_SIZE);
static struct k_thread thread_data;

static metal_phys_addr_t shm_physmap[] = { SHM_START_ADDR };
static struct metal_device shm_device = {
    .name = SHM_DEVICE_NAME,
    .bus = NULL,
    .num_regions = 1,
    .regions = {
        [0] = {
            .virt       = (void *)SHM_START_ADDR,
            .physmap    = shm_physmap,
            .size       = SHM_SIZE,
            .page_shift = (metal_phys_addr_t)(-1),
            .page_mask  = (metal_phys_addr_t)(-1),
            .mem_flags  = 0,
            .ops        = { NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL },
        },
    },
    .node = { NULL },
    .irq_num = 0,
    .irq_info = NULL,
};

static uint8_t rpmsg_recv_buf[RPMSG_BUFFER_SIZE];
static uint8_t rpmsg_send_buf[RPMSG_BUFFER_SIZE];

static struct virtio_vring_info rvrings[RPMSG_NUM_VRINGS] = {
    [0] = {
        .info.align = VRING_ALIGNMENT,
    },
    [1] = {
        .info.align = VRING_ALIGNMENT,
    },
};
static struct virtio_device vdev;
static struct rpmsg_virtio_device rvdev;
static struct metal_io_region *io;
static struct virtqueue *vq[RPMSG_NUM_VRINGS];

static unsigned char virtio_get_status(struct virtio_device *vdev)
{
    return VIRTIO_CONFIG_STATUS_DRIVER_OK;
}

static void virtio_set_status(struct virtio_device *vdev, uint8_t status)
{
    sys_write8(status, VDEV_STATUS_ADDR);
}

static uint32_t virtio_get_features(struct virtio_device *vdev)
{
    return 1 << VIRTIO_RPMSG_F_NS;
}

static void virtio_set_features(struct virtio_device *vdev, uint32_t features)
{
    ARG_UNUSED(vdev);
    ARG_UNUSED(features);
}

static void virtio_notify(struct virtqueue *vq)
{
    ARG_UNUSED(vq);

    ivm_device_notify();
}

struct virtio_dispatch dispatch = {
    .get_status   = virtio_get_status,
    .set_status   = virtio_set_status,
    .get_features = virtio_get_features,
    .set_features = virtio_set_features,
    .notify       = virtio_notify,
};

static K_SEM_DEFINE(data_sem, 0, 1);
static K_SEM_DEFINE(data_rx_sem, 0, 1);

static void evtchn_got_evt(void *data)
{
    ARG_UNUSED(data);

    k_sem_give(&data_sem);
}

int endpoint_cb(struct rpmsg_endpoint *ept, void *data,
                size_t len, uint32_t src, void *priv)
{
    ARG_UNUSED(ept);
    ARG_UNUSED(src);
    ARG_UNUSED(priv);

    memset(rpmsg_recv_buf, 0, sizeof(rpmsg_recv_buf));
    memcpy(rpmsg_recv_buf, data, len);

    k_sem_give(&data_rx_sem);

    return RPMSG_SUCCESS;
}

static K_SEM_DEFINE(ept_sem, 0, 1);

struct rpmsg_endpoint rept;
struct rpmsg_endpoint *ep = &rept;

static void rpmsg_service_unbind(struct rpmsg_endpoint *ept)
{
    ARG_UNUSED(ept);

    rpmsg_destroy_ept(ep);
}

void ns_bind_cb(struct rpmsg_device *rdev, const char *name, uint32_t dest)
{
    (void)rpmsg_create_ept(ep, rdev, name,
                           RPMSG_ADDR_ANY, dest,
                           endpoint_cb,
                           rpmsg_service_unbind);

    k_sem_give(&ept_sem);
}

static void poll_message(void *priv)
{
    while (k_sem_take(&data_rx_sem, K_NO_WAIT) != 0) {
        int status = k_sem_take(&data_sem, K_FOREVER);

        if (status == 0) {
            virtqueue_notification(vq[RPMSG_VRING_IDX_TX]);
        }
    }
}

static int send_message(uint8_t *data, size_t len)
{
    return rpmsg_send(ep, data, len);
}

static struct rpmsg_virtio_shm_pool shpool;

void app_task(void *arg1, void *arg2, void *arg3)
{
    ARG_UNUSED(arg1);
    ARG_UNUSED(arg2);
    ARG_UNUSED(arg3);

    int status = 0;
    unsigned int msg_idx = 0U;
    struct metal_device *device = NULL;
    struct metal_init_params metal_params = METAL_INIT_DEFAULTS;

    metal_params.log_level = METAL_LOG_DEBUG;

    printk("\r\nOpenAMP [RPMsg-Host] demo started.\r\n");

    /*
     * Init ivm_device first of all.
     * The ivm_device must be initialized before metal_init().
     */
    ivm_device_init((void *)evtchn_got_evt);

    status = metal_init(&metal_params);
    if (status != 0) {
        printk("Failed to metal_init: %d\n", status);
        goto _out;
    }

    status = metal_register_generic_device(&shm_device);
    if (status != 0) {
        printk("Failed to register shm device: %d\n", status);
        goto _out;
    }

    status = metal_device_open(IVM_METAL_BUS_NAME, SHM_DEVICE_NAME, &device);
    if (status != 0) {
        printk("Failed to open %s - %s: %d\n",
               IVM_METAL_BUS_NAME, SHM_DEVICE_NAME, status);
        goto _out;
    }

    io = metal_device_io_region(device, 0);
    if (io == NULL) {
        printk("Failed to get IO region\n");
        goto _out;
    }

    /* Setup vdev */
    vq[RPMSG_VRING_IDX_TX] = virtqueue_allocate(VRING_SIZE);
    if (vq[RPMSG_VRING_IDX_TX] == NULL) {
        printk("Failed to allocate virtqueue vq[RPMSG_VRING_IDX_TX]\n");
        goto _free_vq0;
    }
    vq[RPMSG_VRING_IDX_RX] = virtqueue_allocate(VRING_SIZE);
    if (vq[RPMSG_VRING_IDX_RX] == NULL) {
        printk("Failed to allocate virtqueue vq[RPMSG_VRING_IDX_RX]\n");
        goto _free_vq1;
    }

    vdev.role = RPMSG_HOST;
    vdev.vrings_num = VRING_COUNT;
    vdev.func = &dispatch;
    rvrings[RPMSG_VRING_IDX_TX].io = io;
    rvrings[RPMSG_VRING_IDX_TX].info.vaddr = (void *)VRING_TX_ADDRESS;
    rvrings[RPMSG_VRING_IDX_TX].info.num_descs = VRING_SIZE;
    rvrings[RPMSG_VRING_IDX_TX].info.align = VRING_ALIGNMENT;
    rvrings[RPMSG_VRING_IDX_TX].vq = vq[RPMSG_VRING_IDX_TX];

    rvrings[RPMSG_VRING_IDX_RX].io = io;
    rvrings[RPMSG_VRING_IDX_RX].info.vaddr = (void *)VRING_RX_ADDRESS;
    rvrings[RPMSG_VRING_IDX_RX].info.num_descs = VRING_SIZE;
    rvrings[RPMSG_VRING_IDX_RX].info.align = VRING_ALIGNMENT;
    rvrings[RPMSG_VRING_IDX_RX].vq = vq[RPMSG_VRING_IDX_RX];

    vdev.vrings_info = &rvrings[0];

    /* Setup rvdev */
    rpmsg_virtio_init_shm_pool(&shpool, (void *)SHM_START_ADDR, SHM_SIZE);
    status = rpmsg_init_vdev(&rvdev, &vdev, ns_bind_cb, io, &shpool);
    if (status != 0) {
        printk("Failed to init vdev: %d\n", status);
        goto _cleanup;
    }

    /* Since we are using name service, we need to wait for a response
     * from NS setup and than we need to process it
     */
    k_sem_take(&data_sem, K_FOREVER);
    virtqueue_notification(vq[RPMSG_VRING_IDX_TX]);

    /* Wait til nameservice ep is setup */
    k_sem_take(&ept_sem, K_FOREVER);

    /*
     * Demo #1
     * Send messages to remote and get echo reply
     */
    while (msg_idx < DEMO1_MSG_LIMIT) {
        snprintf((char *)rpmsg_send_buf, sizeof(rpmsg_send_buf),
                 "ping - %02u", msg_idx);
        status = send_message(rpmsg_send_buf, strlen((char *)rpmsg_send_buf));
        if (status < 0) {
            printk("Failed to send message(%u): %d\n", msg_idx, status);
            goto _cleanup;
        }

        poll_message(NULL);
        printk("Received message: %s\n", (char *)rpmsg_recv_buf);

        msg_idx++;
    }

    /*
     * Demo #2
     * Send data to remote periodically (no reply)
     */
    while (1) {
        struct payload pd;
        k_thread_runtime_stats_t rt_stats_thread;

        k_thread_runtime_stats_get(k_current_get(), &rt_stats_thread);

        memset(&pd, 0, sizeof(pd));
        pd.magic = MAGIC;
        pd.id = sample_idx;
        pd.timestamp = sys_clock_tick_get();
        pd.type = 0;
        pd.data.dwords[0] = k_uptime_get_32();  /* system uptime in ms */
        pd.data.dwords[1] = k_cycle_get_32();   /* hardware clock (cycles) */
        pd.data.qwords[1] = rt_stats_thread.execution_cycles;
        pd.data.qwords[2] = rt_stats_thread.peak_cycles;
        pd.data.qwords[3] = rt_stats_thread.average_cycles;
        pd.checksum = 0x55AA;   /* Reserved for checksum using CRC16 */
        status = send_message((uint8_t *)&pd, sizeof(pd));
        if (status < 0) {
            printk("Failed to send_payload(%lu): %d\n", sample_idx, status);
            break;
        }
        printk("Sent payload: idx - %u, uptime - %u, cycles - %u, "
               "thread: total - %llu, peak - %llu, avg - %llu\n",
               pd.id, pd.data.dwords[0], pd.data.dwords[1],
               pd.data.qwords[1], pd.data.qwords[2], pd.data.qwords[3]);
        sample_idx++;
        k_sleep(K_SECONDS(5));
    }

_cleanup:
    rpmsg_deinit_vdev(&rvdev);

_free_vq1:
    virtqueue_free(vq[RPMSG_VRING_IDX_RX]);
_free_vq0:
    virtqueue_free(vq[RPMSG_VRING_IDX_TX]);

_out:
    metal_finish();

    printk("OpenAMP demo ended.\n");
}

void main(void)
{
    printk("Starting application ...\n");

    k_thread_create(&thread_data,
                    thread_stack,
                    APP_TASK_STACK_SIZE,
                    (k_thread_entry_t)app_task,
                    NULL, NULL, NULL,
                    K_PRIO_COOP(7), 0, K_NO_WAIT);
}

int init_status_flag(const struct device *arg)
{
    ARG_UNUSED(arg);

    virtio_set_status(NULL, 0);

    return 0;
}

SYS_INIT(init_status_flag, PRE_KERNEL_1, CONFIG_KERNEL_INIT_PRIORITY_DEFAULT);
