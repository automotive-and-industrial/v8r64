/*
 * Copyright (c) 2022, Arm Limited.
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#include <openamp/open_amp.h>
#include <metal/alloc.h>
#include <metal/device.h>

#include <zephyr/xen/events.h>

#include "config.h"
#include "device.h"

extern int metal_generic_dev_open(struct metal_bus *bus, const char *dev_name,
                                  struct metal_device **device);

static struct ivm_device ivmdev = {
    .name = IVM_DEV_NAME,
    .bus = NULL,
    .shmem_dev = {
        .name = SHM_DEVICE_NAME,
        .size = SHMEM_REGION_SIZE,
        .base_phys_addr = SHMEM_BASE_ADDRESS,
    },
    .evtchn_dev = {
        .name = EVTCHN_DEVICE_NAME,
        .port = EVTCHN_PORT_DOM_ZEPHYR,
    },
    .vdev_info = {
        .size = VDEV_SIZE,
        .status_size = VDEV_STATUS_SIZE,
    },
};

/*
 * The variable metal_generic_bus is declared in
 * libmetal/lib/system/zephyr/device.c
 * and used in metal_bus_register() by metal_init().
 * We reuse it and overwrite its fields to our own in ivm_device_int().
 */
extern struct metal_bus metal_generic_bus;

/*
 * This function need to be called before metal_init()
 */
int ivm_device_init(void *data)
{
    metal_generic_bus.name = IVM_METAL_BUS_NAME;
    metal_generic_bus.ops.dev_open = ivm_device_open;
    metal_generic_bus.ops.dev_close = ivm_device_close;
    metal_generic_bus.ops.dev_irq_ack = NULL;
    metal_generic_bus.ops.dev_dma_map = NULL;
    metal_generic_bus.ops.dev_dma_unmap = NULL;

    /*
     * The priv field is used to store the callback function of event channel
     */
    ivmdev.priv = data;

    return 0;
}

int ivm_device_open(struct metal_bus *bus, const char *dev_name,
                    struct metal_device **device)
{
    /* Setup evtchn */
    bind_event_channel(ivmdev.evtchn_dev.port, (evtchn_cb_t)ivmdev.priv, NULL);

    return metal_generic_dev_open(bus, dev_name, device);
}

void ivm_device_close(struct metal_bus *bus, struct metal_device *device)
{
    (void)bus;
    (void)device;

    unbind_event_channel(ivmdev.evtchn_dev.port);
}

int ivm_device_notify(void)
{
    notify_evtchn(ivmdev.evtchn_dev.port);

    return 0;
}

int ivm_device_wait_event(void)
{
    /* Reserved */

    return 0;
}

struct ivm_shmem_device *ivm_device_get_shmem_dev(void)
{
    return &ivmdev.shmem_dev;
}

struct ivm_evtchn_device *ivm_device_get_evtchn_dev(void)
{
    return &ivmdev.evtchn_dev;
}

struct ivm_vdev_info *ivm_device_get_vdev_info(void)
{
    return &ivmdev.vdev_info;
}

struct ivm_shm_pool_info *ivm_device_get_shm_pool_info(void)
{
    return &ivmdev.shm_pool_info;
}

struct ivm_vring_info *ivm_device_get_vring_info(void)
{
    return &ivmdev.vring_info;
}
