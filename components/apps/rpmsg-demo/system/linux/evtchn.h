/*
 * Copyright (c) 2022, Arm Limited.
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __EVTCHN_H__
#define __EVTCHN_H__

#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/ioctl.h>
#include <xen/xen.h>
#include <xen/evtchn.h>

#include "config.h"

#define EVTCHN_DEV_NAME     EVTCHN_DEVICE_NAME

#ifndef IOCTL_EVTCHN_BIND
/*
 * Bind statically allocated @port.
 */
struct ioctl_evtchn_bind {
    unsigned int port;
};

#define IOCTL_EVTCHN_BIND   \
    _IOC(_IOC_NONE, 'E', 7, sizeof(struct ioctl_evtchn_bind))
#endif

int evtchn_open(void);
void evtchn_close(int fd);
int evtchn_restrict(uint16_t domid);
int evtchn_notify(evtchn_port_t port);
int evtchn_bind(evtchn_port_t port);
int evtchn_bind_unbound_port(uint32_t domid);
int evtchn_bind_interdomain(uint32_t remote_domid, evtchn_port_t remote_port);
int evtchn_bind_virq(unsigned int virq);
int evtchn_unbind(evtchn_port_t port);
int evtchn_pending(void);
int evtchn_unmask(evtchn_port_t port);

#endif /* __EVTCHN_H__ */
