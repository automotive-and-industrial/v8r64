/*
 * Copyright (c) 2022, Arm Limited.
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <poll.h>
#include <assert.h>

#include <openamp/open_amp.h>
#include <metal/alloc.h>
#include <metal/device.h>

#include "config.h"
#include "shmem.h"
#include "evtchn.h"
#include "device.h"

extern int metal_generic_dev_open(struct metal_bus *bus, const char *dev_name,
                                  struct metal_device **device);

static struct ivm_device ivmdev = {
    .name = IVM_DEV_NAME,
    .bus = NULL,
    .shmem_dev = {
        .name = SHMEM_DEV_NAME,
        .size = SHMEM_REGION_SIZE,
        .base_phys_addr = SHMEM_BASE_ADDRESS,
    },
    .evtchn_dev = {
        .name = EVTCHN_DEV_NAME,
        .port = EVTCHN_PORT_DOM_LINUX,
    },
    .vdev_info = {
        .size = VDEV_SIZE,
        .status_size = VDEV_STATUS_SIZE,
    },
};

int ivm_device_init(void *data)
{
    (void)data;

    return 0;
}

int ivm_device_open(struct metal_bus *bus, const char *dev_name,
                    struct metal_device **device)
{
    int ret = -1;
    int shmem_fd, evtchn_fd;
    void *mmap_addr = NULL;

    ret = metal_generic_dev_open(bus, dev_name, device);
    if (ret < 0) {
        printf("Failed to open generic metal device: %d\n", ret);
        return ret;
    }

    shmem_fd = shmem_open();
    if (shmem_fd < 0) {
        printf("Failed to open shmem device: %d\n", shmem_fd);
        return -1;
    }
    ivmdev.shmem_dev.fd = shmem_fd;

    mmap_addr = shmem_mmap(ivmdev.shmem_dev.size);
    if (mmap_addr == MAP_FAILED) {
        goto _exit_shmem;
    }
    ivmdev.shmem_dev.base_virt_addr = (unsigned long)mmap_addr;

    evtchn_fd = evtchn_open();
    if (evtchn_fd < 0) {
        printf("Failed to open evtchn device: %d\n", evtchn_fd);
        goto _exit_shmem;
    }
    ivmdev.evtchn_dev.fd = evtchn_fd;

    ret = evtchn_bind(ivmdev.evtchn_dev.port);
    if (ret < 0) {
        printf("Failed to bind evtchn port %u\n", ivmdev.evtchn_dev.port);
        goto _exit_evtchn;
    }
    ivmdev.evtchn_dev.pfd.fd = evtchn_fd;
    ivmdev.evtchn_dev.pfd.events = POLLIN;

    ivmdev.vdev_info.start_addr = ivmdev.shmem_dev.base_virt_addr;
    ivmdev.vdev_info.status_addr = ivmdev.vdev_info.start_addr;

    ivmdev.shm_pool_info.start_addr = ivmdev.vdev_info.start_addr + \
                                      ivmdev.vdev_info.status_size;
    ivmdev.shm_pool_info.size = ivmdev.vdev_info.size - \
                                ivmdev.vdev_info.status_size;

    ivmdev.vring_info.rx_addr = ivmdev.vdev_info.start_addr + \
                                ivmdev.shm_pool_info.size - \
                                ivmdev.vdev_info.status_size;
    ivmdev.vring_info.tx_addr = ivmdev.vdev_info.start_addr + \
                                ivmdev.shm_pool_info.size;

    (*device)->regions[0].virt = (void *)ivmdev.shm_pool_info.start_addr;

    return 0;

_exit_evtchn:
    evtchn_close(evtchn_fd);
_exit_shmem:
    shmem_close(shmem_fd);

    return -1;
}

void ivm_device_close(struct metal_bus *bus, struct metal_device *device)
{
    (void)bus;
    (void)device;

    evtchn_unbind(ivmdev.evtchn_dev.port);
    shmem_munmap((void *)ivmdev.shmem_dev.base_virt_addr,
                 ivmdev.shmem_dev.size);

    evtchn_close(ivmdev.evtchn_dev.fd);
    shmem_close(ivmdev.shmem_dev.fd);
}

int ivm_device_notify(void)
{
    return evtchn_notify(ivmdev.evtchn_dev.port);
}

int ivm_device_wait_event(void)
{
    struct pollfd *ppfd = &ivmdev.evtchn_dev.pfd;
    evtchn_port_t expect = ivmdev.evtchn_dev.port;
    evtchn_port_t got = (evtchn_port_t)(-1);
    int ret;

    ret = poll(ppfd, 1, -1);
    if (ret < 0) {
        printf("Failed to poll evtchn\n");
        return -1;
    }
    if (ppfd->revents & POLLIN) {
        got = evtchn_pending();
    }


    if (got != expect) {
        printf("Event from port [%d], but expect [%d]\n",
               got, expect);

        return -1;
    }

    /* Set event as served */
    evtchn_unmask(expect);

    return 0;
}

struct ivm_shmem_device *ivm_device_get_shmem_dev(void)
{
    return &ivmdev.shmem_dev;
}

struct ivm_evtchn_device *ivm_device_get_evtchn_dev(void)
{
    return &ivmdev.evtchn_dev;
}

struct ivm_vdev_info *ivm_device_get_vdev_info(void)
{
    return &ivmdev.vdev_info;
}

struct ivm_shm_pool_info *ivm_device_get_shm_pool_info(void)
{
    return &ivmdev.shm_pool_info;
}

struct ivm_vring_info *ivm_device_get_vring_info(void)
{
    return &ivmdev.vring_info;
}


struct metal_bus metal_generic_bus = {
    .name = IVM_METAL_BUS_NAME,
    .ops = {
        .dev_open = ivm_device_open,
        .dev_close = ivm_device_close,
        .dev_irq_ack = NULL,
        .dev_dma_map = NULL,
        .dev_dma_unmap = NULL,
    },
};
