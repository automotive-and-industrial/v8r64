/*
 * Copyright (c) 2022, Arm Limited.
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __SHMEM_H__
#define __SHMEM_H__

#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>

#include "config.h"

#define SHMEM_DEV_NAME      SHM_DEVICE_NAME

int shmem_open(void);
void shmem_close(int fd);
void *shmem_mmap(size_t length);
int shmem_munmap(void *addr, size_t length);

#endif /* __SHMEM_H__ */
