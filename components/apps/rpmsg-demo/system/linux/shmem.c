/*
 * Copyright (c) 2022, Arm Limited.
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#include "shmem.h"

#define SHMEM_TEST_GET_FD   0

static int _shmem_fd = -1;

int shmem_open(void)
{
    _shmem_fd = open(SHMEM_DEV_NAME, 0);

    return _shmem_fd;
}

void shmem_close(int fd)
{
    (void)fd;

    if (_shmem_fd < 0) {
        return ;
    }

    close(_shmem_fd);
}

void *shmem_mmap(size_t length)
{
    int fd;

    if (ioctl(_shmem_fd, SHMEM_TEST_GET_FD, &fd) < 0) {
        return MAP_FAILED;
    }

    return mmap(NULL, length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
}

int shmem_munmap(void *addr, size_t length)
{
    /* NOT SUPPORTED YET, RESERVED FOR FUTURE */
    if (!addr || !length) {
        return -EINVAL;
    }

    return munmap(addr, length);
}
