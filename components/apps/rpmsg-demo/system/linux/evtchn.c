/*
 * Copyright (c) 2022, Arm Limited.
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "evtchn.h"

static int _evtchn_fd = -1;

int evtchn_open(void)
{
    _evtchn_fd = open(EVTCHN_DEV_NAME, O_RDWR);

    return _evtchn_fd;
}

void evtchn_close(int fd)
{
    (void)fd;

    if (_evtchn_fd < 0) {
        return ;
    }

    close(_evtchn_fd);
}

int evtchn_restrict(uint16_t domid)
{
    struct ioctl_evtchn_restrict_domid restrict_domid = { domid };

    return ioctl(_evtchn_fd, IOCTL_EVTCHN_RESTRICT_DOMID, &restrict_domid);
}

int evtchn_notify(evtchn_port_t port)
{
    struct ioctl_evtchn_notify notify;

    notify.port = port;

    return ioctl(_evtchn_fd, IOCTL_EVTCHN_NOTIFY, &notify);
}

int evtchn_bind(evtchn_port_t port)
{
    struct ioctl_evtchn_bind bind;

    bind.port = port;

    return ioctl(_evtchn_fd, IOCTL_EVTCHN_BIND, &bind);
}

int evtchn_bind_unbound_port(uint32_t domid)
{
    struct ioctl_evtchn_bind_unbound_port bind;

    bind.remote_domain = domid;

    return ioctl(_evtchn_fd, IOCTL_EVTCHN_BIND_UNBOUND_PORT, &bind);
}

int evtchn_bind_interdomain(uint32_t remote_domid, evtchn_port_t remote_port)
{
    struct ioctl_evtchn_bind_interdomain bind;

    bind.remote_domain = remote_domid;
    bind.remote_port = remote_port;

    return ioctl(_evtchn_fd, IOCTL_EVTCHN_BIND_INTERDOMAIN, &bind);
}

int evtchn_bind_virq(unsigned int virq)
{
    struct ioctl_evtchn_bind_virq bind;

    bind.virq = virq;

    return ioctl(_evtchn_fd, IOCTL_EVTCHN_BIND_VIRQ, &bind);
}

int evtchn_unbind(evtchn_port_t port)
{
    struct ioctl_evtchn_unbind unbind;

    unbind.port = port;

    return ioctl(_evtchn_fd, IOCTL_EVTCHN_UNBIND, &unbind);
}

int evtchn_pending(void)
{
    evtchn_port_t port;

    if (read(_evtchn_fd, &port, sizeof(port)) != sizeof(port)) {
        return -1;
    }

    return port;
}

int evtchn_unmask(evtchn_port_t port)
{
    if (write(_evtchn_fd, &port, sizeof(port)) != sizeof(port)) {
        return -1;
    }

    return 0;
}
