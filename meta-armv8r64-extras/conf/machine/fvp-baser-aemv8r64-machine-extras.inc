# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# Machine-specific configuration for the fvp-baser-aemv8r64 MACHINE

ZEPHYR_INHERIT_CLASSES:append = " zephyr-machine-extras"

PREFERRED_VERSION_linux-yocto = "5.19%"
PREFERRED_VERSION_zephyr-kernel = "3.2.0"
PREFERRED_VERSION_xen = "4.17+stable%"
PREFERRED_VERSION_xen-tools = "4.17+stable%"
PREFERRED_PROVIDER_libmetal = "libmetal"
