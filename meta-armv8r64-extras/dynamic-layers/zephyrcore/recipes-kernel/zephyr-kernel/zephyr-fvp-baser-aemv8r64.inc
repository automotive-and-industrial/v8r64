# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

inherit zephyr-fvpboot zephyr-testimage

COMPATIBLE_MACHINE = "fvp-baser-aemv8r64"

ZEPHYR_NUM_CPUS ??= "4"
ZEPHYR_BOARD = "${@ 'fvp_baser_aemv8r' if d.getVar('ZEPHYR_NUM_CPUS') == '1' \
    else 'fvp_baser_aemv8r_smp' }"

EXTRA_OECMAKE:append = " \
    -DCMAKE_C_FLAGS=-fno-stack-protector \
    -DCMAKE_LD_FLAGS=-fno-stack-protector \
    -DCONFIG_MP_NUM_CPUS=${ZEPHYR_NUM_CPUS} \
    "
