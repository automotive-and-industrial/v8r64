# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

ZEPHYR_NUM_CPUS:fvp-baser-aemv8r64 = "1"
