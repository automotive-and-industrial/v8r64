# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

require recipes-devtools/zephyr-sdk/zephyr-sdk_0.14.2.bb

# Installing the SDK on the target is not supported
COMPATIBLE_MACHINE:class-target = "unset"

SRC_URI[x86_64.sha256sum] = "0a7406045102197b9edc759b242499941814a1c6df29dd9fbd479ad50eb0fba9"
SRC_URI[aarch64.sha256sum] = "d2c5de994376a287e8bf12e5776ab8f5105eafa14826608df085142fd01b7f84"
