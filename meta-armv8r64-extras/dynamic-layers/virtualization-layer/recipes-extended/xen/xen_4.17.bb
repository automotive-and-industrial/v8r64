# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

require recipes-extended/xen/xen_4.16.bb

SRCREV = "11560248ffda3f00f20bbdf3ae088af474f7f2a3"
XEN_REL = "4.17"

SRC_URI:remove = " file://xen-fix-gcc12-build-issues.patch"

LIC_FILES_CHKSUM = "file://COPYING;md5=d1a1e216f80b6d8da95fec897d0dbec9"
