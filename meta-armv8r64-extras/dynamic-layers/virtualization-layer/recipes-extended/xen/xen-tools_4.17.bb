# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

require recipes-extended/xen/xen-tools_4.16.bb

SRCREV = "14dd241aad8af447680ac73e8579990e2c09c1e7"
XEN_REL = "4.17"

SRC_URI:remove = " file://xen-fix-gcc12-build-issues.patch"

LIC_FILES_CHKSUM = "file://COPYING;md5=419739e325a50f3d7b4501338e44a4e5"
