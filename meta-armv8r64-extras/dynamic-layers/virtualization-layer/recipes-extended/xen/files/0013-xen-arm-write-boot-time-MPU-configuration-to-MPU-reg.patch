From 527c31fd7e815b7c297358be069e8567c58c18ec Mon Sep 17 00:00:00 2001
From: Penny Zheng <penny.zheng@arm.com>
Date: Tue, 17 May 2022 20:05:47 +0800
Subject: [PATCH 13/47] xen/arm: write boot-time MPU configuration to MPU
 registers

We have created a boot-time MPU configuration for Xen different
sections and early UART (if configured) in memory. But this
configuration will not take effect until we have written it
to MPU protection region registers and enable MPU.

So in this patch, we will write this boot-time configuration
to MPU hardware. But Xen text and data are in an enabled MPU
protection region, we can't disable this protection region
and update it (disable a protection region where the accessing
data is located will cause unpredictable behavior). So we have
to disable the whole MPU hardware and update the protection
regions. And then re-enable the MPU hardware.

After MPU is disabled, in the worst case, the default background
MPU attributes will be set to device memory. To avoid unexpected
unaligment access fault during MPU disabled, we will use assembly
code to update MPU protection region registers.

And like MMU system force WNX after setting pagetables to MMU
hardware, we also force WNX after setting MPU configuration
to MPU hardware. To avoid to many #ifdef in start_xen, we use
setup_mm_data for MMU and MPU systems to setup boot-time pagetables
and protection regions.

Issue-Id: SCM-4571
Change-Id: I4a98be605b022074dcca445ad5db5efb86f5ac03
Signed-off-by: Wei Chen <wei.chen@arm.com>
Signed-off-by: Penny Zheng <penny.zheng@arm.com>
Upstream-Status: Inappropriate [other]
  Implementation pending further discussion
Signed-off-by: Robbie Cao <robbie.cao@arm.com>
---
 xen/arch/arm/arm64/head_mpu.S     | 19 +++++++++++
 xen/arch/arm/include/asm/mm_mmu.h |  2 +-
 xen/arch/arm/include/asm/mm_mpu.h |  9 ++++++
 xen/arch/arm/mm_mmu.c             |  2 +-
 xen/arch/arm/mm_mpu.c             | 52 +++++++++++++++++++++++++++++++
 xen/arch/arm/setup.c              |  2 +-
 6 files changed, 83 insertions(+), 3 deletions(-)

diff --git a/xen/arch/arm/arm64/head_mpu.S b/xen/arch/arm/arm64/head_mpu.S
index 336c0a630f..658a06c550 100644
--- a/xen/arch/arm/arm64/head_mpu.S
+++ b/xen/arch/arm/arm64/head_mpu.S
@@ -152,3 +152,22 @@ ENTRY(disable_mm)
     isb
     ret
 ENDPROC(disable_mm)
+
+/*
+ * Iterate MPU memory region configuration, #mpu_table, to properly
+ * set according MPU protection region registers.
+ *
+ * x0: len refers to table length.
+ * x1: mpu_table. Every entry(pr_t) takes up 16 bytes, the previous 8 bytes
+ * shall be stored in prbar register, and the latter 8 are for prlar register.
+ */
+ENTRY(set_boot_mpumap)
+    mov   x2, xzr               /* table index */
+1:  ldr   x3, [x1], #8          /* x3: prbar */
+    ldr   x4, [x1], #8          /* x4: prlar */
+    write_pr x2, x3, x4
+    add   x2, x2, #1            /* table index ++ */
+    cmp   x2, x0
+    b.lt  1b
+    ret
+ENDPROC(set_boot_mpumap)
diff --git a/xen/arch/arm/include/asm/mm_mmu.h b/xen/arch/arm/include/asm/mm_mmu.h
index 6133c23b80..37ca8e375b 100644
--- a/xen/arch/arm/include/asm/mm_mmu.h
+++ b/xen/arch/arm/include/asm/mm_mmu.h
@@ -4,7 +4,7 @@
 #define frame_table ((struct page_info *)FRAMETABLE_VIRT_START)
 
 /* Boot-time pagetable setup */
-extern void setup_pagetables(unsigned long boot_phys_offset);
+extern void setup_mm_data(unsigned long boot_phys_offset);
 /* Allocate and initialise pagetables for a secondary CPU. Sets init_ttbr to the
  * new page table */
 extern int init_secondary_pagetables(int cpu);
diff --git a/xen/arch/arm/include/asm/mm_mpu.h b/xen/arch/arm/include/asm/mm_mpu.h
index ce8cfcffaf..834bd874d9 100644
--- a/xen/arch/arm/include/asm/mm_mpu.h
+++ b/xen/arch/arm/include/asm/mm_mpu.h
@@ -25,6 +25,15 @@
 /* MPU-related varaible */
 extern unsigned long nr_xen_mpumap;
 
+/* Boot-time MPU protection region configuration setup */
+extern void setup_protection_regions(void);
+#define setup_mm_data(x) setup_protection_regions()
+
+/* MPU-related functionality */
+extern void enable_mm(void);
+extern void disable_mm(void);
+extern void set_boot_mpumap(u64 len, pr_t *table);
+
 static inline paddr_t __virt_to_maddr(vaddr_t va)
 {
     return (paddr_t)va;
diff --git a/xen/arch/arm/mm_mmu.c b/xen/arch/arm/mm_mmu.c
index 4d55551fa2..9fe108626d 100644
--- a/xen/arch/arm/mm_mmu.c
+++ b/xen/arch/arm/mm_mmu.c
@@ -435,7 +435,7 @@ static void clear_table(void *table)
 
 /* Boot-time pagetable setup.
  * Changes here may need matching changes in head.S */
-void __init setup_pagetables(unsigned long boot_phys_offset)
+void __init setup_mm_data(unsigned long boot_phys_offset)
 {
     uint64_t ttbr;
     lpae_t pte, *p;
diff --git a/xen/arch/arm/mm_mpu.c b/xen/arch/arm/mm_mpu.c
index 2cf8051aed..d4598622cb 100644
--- a/xen/arch/arm/mm_mpu.c
+++ b/xen/arch/arm/mm_mpu.c
@@ -121,6 +121,21 @@ static inline pr_t pr_of_xenaddr(paddr_t baddr, paddr_t eaddr, unsigned attr)
     return region;
 }
 
+/*
+ * After boot, Xen memory mapping should not contain mapping that are
+ * both writable and executable.
+ *
+ * This should be called on each CPU to enforce the policy like MMU
+ * system. But the different is that, for MPU systems, EL2 stage 1
+ * PMSAv8-64 attributes will not be cached by TLB (ARM DDI 0600A.c
+ * D1.6.2 TLB maintenance instructions). So this MPU version function
+ * does not need Xen local TLB flush.
+ */
+static void xen_mpu_enforce_wnx(void)
+{
+    WRITE_SYSREG(READ_SYSREG(SCTLR_EL2) | SCTLR_Axx_ELx_WXN, SCTLR_EL2);
+}
+
 /*
  * At boot-time, there are only two MPU memory regions defined: normal memory
  * and device memory, which are now insecure and coarse-grained.
@@ -203,3 +218,40 @@ static void __init map_xen_to_protection_regions(void)
 
     nr_xen_mpumap = next_xen_mpumap_index;
 }
+
+void __init setup_protection_regions()
+{
+    map_xen_to_protection_regions();
+
+    /*
+     * MPU must be disabled to switch to new mpu memory region configuration.
+     * Once the MPU is disabled, cache should also be disabled, because if MPU
+     * is disabled, some system will treat the memory access as the IO memory
+     * access. The cache data should be flushed to RAM before disabling MPU.
+     */
+    clean_dcache_va_range((void *)&next_xen_mpumap_index,
+                          sizeof(unsigned long));
+    clean_dcache_va_range((void *)(pr_t *)boot_mpumap,
+                          sizeof(pr_t) * next_xen_mpumap_index);
+
+    /*
+     * Since it is the MPU protection region which holds the XEN kernel that
+     * needs updating.
+     * The whole MPU system must be disabled for the update.
+     */
+    disable_mm();
+
+    /*
+     * Set new MPU memory region configuration.
+     * To avoid the mismatch between nr_xen_mpumap and nr_xen_mpumap
+     * after the relocation of some MPU regions later, here
+     * next_xen_mpumap_index is used.
+     * To avoid unexpected unaligment access fault during MPU disabled,
+     * set_boot_mpumap shall be written in assembly code.
+     */
+    set_boot_mpumap(next_xen_mpumap_index, (pr_t *)boot_mpumap);
+
+    enable_mm();
+
+    xen_mpu_enforce_wnx();
+}
diff --git a/xen/arch/arm/setup.c b/xen/arch/arm/setup.c
index 28a23a3149..e898c8a7d9 100644
--- a/xen/arch/arm/setup.c
+++ b/xen/arch/arm/setup.c
@@ -1015,7 +1015,7 @@ void __init start_xen(unsigned long boot_phys_offset,
     /* Initialize traps early allow us to get backtrace when an error occurred */
     init_traps();
 
-    setup_pagetables(boot_phys_offset);
+    setup_mm_data(boot_phys_offset);
 
     smp_clear_cpu_maps();
 
-- 
2.25.1

