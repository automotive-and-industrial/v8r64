From 6ed5c486d3b423fba5eeb862ebcd78e99bc73968 Mon Sep 17 00:00:00 2001
From: Wei Chen <wei.chen@arm.com>
Date: Sun, 30 Oct 2022 18:56:19 +0800
Subject: [PATCH 35/47] xen/arm: prepare SMP support for MPU systems

Before this patch, Xen only do initialization on boot cpu.
In this patch we will invoke smp_function_call to ask the
secondary CPUs to do initialization.

Change-Id: I4be125040d913a9f34d7db36c4f400139ef72394
Signed-off-by: Wei Chen <wei.chen@arm.com>
Upstream-Status: Inappropriate [other]
  Implementation pending further discussion
Signed-off-by: Robbie Cao <robbie.cao@arm.com>
---
 xen/arch/arm/arm64/head.S         | 12 ++++++++
 xen/arch/arm/include/asm/mm_mpu.h | 11 ++++++--
 xen/arch/arm/mm_mpu.c             | 47 +++++++++++++++++++++++++++++++
 xen/arch/arm/p2m_mpu.c            |  1 +
 xen/arch/arm/setup_mpu.c          | 11 ++++++++
 5 files changed, 80 insertions(+), 2 deletions(-)

diff --git a/xen/arch/arm/arm64/head.S b/xen/arch/arm/arm64/head.S
index 228f01db69..bba7e05fad 100644
--- a/xen/arch/arm/arm64/head.S
+++ b/xen/arch/arm/arm64/head.S
@@ -313,6 +313,17 @@ GLOBAL(init_secondary)
 #endif
         bl    check_cpu_mode
         bl    cpu_init
+#ifdef CONFIG_HAS_MPU
+        ldr   x0, =next_xen_mpumap_index
+        ldr   x0, [x0]
+        ldr   x1, =xen_mpumap
+        ldr   x1, [x1]
+        dsb   sy
+        bl    set_boot_mpumap
+        bl    enable_mm
+        dsb   sy
+        isb
+#else
         bl    prepare_early_mappings
         bl    enable_mm
 
@@ -335,6 +346,7 @@ secondary_switched:
         tlbi  alle2
         dsb   sy                     /* Ensure completion of TLB flush */
         isb
+#endif /* !CONFIG_HAS_MPU */
 
 #ifdef CONFIG_EARLY_PRINTK
         /* Use a virtual address to access the UART. */
diff --git a/xen/arch/arm/include/asm/mm_mpu.h b/xen/arch/arm/include/asm/mm_mpu.h
index f28d0317a1..11914c8757 100644
--- a/xen/arch/arm/include/asm/mm_mpu.h
+++ b/xen/arch/arm/include/asm/mm_mpu.h
@@ -29,6 +29,13 @@ extern bool heap_parsed;
 /* Helper to access MPU protection region */
 extern void access_protection_region(bool read, pr_t *pr_read,
                                      const pr_t *pr_write, u64 sel);
+/*
+ * Switch secondary CPUS to its own mpu memory configuration and
+ * finalise MPU setup
+ */
+extern int init_secondary_protection_regions(int cpu);
+extern void mpu_init_secondary_cpu(void);
+
 /* MPU-related varaible */
 extern pr_t *xen_mpumap;
 extern unsigned long nr_xen_mpumap;
@@ -64,8 +71,8 @@ uint8_t load_mpu_supported_region_el1(void);
 void save_el1_mpu_regions(pr_t *pr);
 void restore_el1_mpu_regions(pr_t *pr);
 
-#define INIT_SECONDARY_MM_DATA(cpu) (0)
-#define MM_INIT_SECONDARY_CPU() do {} while ( 0 )
+#define INIT_SECONDARY_MM_DATA(cpu) init_secondary_protection_regions(cpu)
+#define MM_INIT_SECONDARY_CPU() mpu_init_secondary_cpu()
 
 static inline paddr_t __virt_to_maddr(vaddr_t va)
 {
diff --git a/xen/arch/arm/mm_mpu.c b/xen/arch/arm/mm_mpu.c
index 8cbeeb85e3..be420425c6 100644
--- a/xen/arch/arm/mm_mpu.c
+++ b/xen/arch/arm/mm_mpu.c
@@ -417,6 +417,34 @@ static void clear_boot_mpumap(void)
                                          ARM_DEFAULT_MPU_PROTECTION_REGIONS);
 }
 
+int init_secondary_protection_regions(int cpu)
+{
+    clear_boot_mpumap();
+
+    /* All CPUs share a single Xen stage 1 MPU memory region configuration. */
+    clean_dcache_va_range((void *)&next_xen_mpumap_index,
+                          sizeof(unsigned long));
+    clean_dcache_va_range((void *)(pr_t *)xen_mpumap,
+                          sizeof(pr_t) * next_xen_mpumap_index);
+    return 0;
+}
+
+/*
+ * Below functions need MPU-specific implementation.
+ * TODO: Implementation on first usage.
+ */
+void __init remove_early_mappings(void)
+{
+}
+
+int map_pages_to_xen(unsigned long virt,
+                     mfn_t mfn,
+                     unsigned long nr_mfns,
+                     unsigned int flags)
+{
+    return 0;
+}
+
 void disable_mpu_region_from_index(unsigned int index)
 {
     pr_t pr = {};
@@ -809,6 +837,7 @@ int reorder_xen_mpumap(void)
                           sizeof(pr_t) * reordered_mpu_index);
 
     reorder_xen_mpumap_one(NULL);
+    smp_call_function(reorder_xen_mpumap_one, NULL, 1);
 
     /* Now, xen_mpumap acts in a tight way, absolutely no holes in there,
      * then always next_xen_mpumap_index = nr_xen_mpumap for later on.
@@ -907,6 +936,11 @@ static void xen_mpu_enforce_wnx(void)
     WRITE_SYSREG(READ_SYSREG(SCTLR_EL2) | SCTLR_Axx_ELx_WXN, SCTLR_EL2);
 }
 
+void mpu_init_secondary_cpu(void)
+{
+    xen_mpu_enforce_wnx();
+}
+
 /*
  * At boot-time, there are only two MPU memory regions defined: normal memory
  * and device memory, which are now insecure and coarse-grained.
@@ -1132,6 +1166,16 @@ void __init update_mm(void)
     map_boot_module_section();
 }
 
+static DECLARE_BITMAP(initial_section_mask, MAX_MPU_PROTECTION_REGIONS);
+static void free_init_memory_one(void *data)
+{
+    unsigned int i;
+
+    for_each_set_bit( i, (const unsigned long *)&initial_section_mask,
+                      MAX_MPU_PROTECTION_REGIONS )
+        disable_mpu_region_from_index(i);
+}
+
 void free_init_memory(void)
 {
     /* Kernel init text section. */
@@ -1175,7 +1219,10 @@ void free_init_memory(void)
         rc = destroy_xen_mappings(init_section[i], init_section[i + 1]);
         if ( rc < 0 )
             panic("Unable to remove the init section (rc = %d)\n", rc);
+        set_bit(rc, initial_section_mask);
     }
+
+    smp_call_function(free_init_memory_one, NULL, 1);
 }
 
 /* Loads and returns the number of EL1 MPU supported regions by the hardware */
diff --git a/xen/arch/arm/p2m_mpu.c b/xen/arch/arm/p2m_mpu.c
index 5de7092537..85beeea138 100644
--- a/xen/arch/arm/p2m_mpu.c
+++ b/xen/arch/arm/p2m_mpu.c
@@ -726,6 +726,7 @@ void __init setup_virt_paging(void)
     vtcr = val;
 
     setup_virt_paging_one(NULL);
+    smp_call_function(setup_virt_paging_one, NULL, 1);
 
     return;
 
diff --git a/xen/arch/arm/setup_mpu.c b/xen/arch/arm/setup_mpu.c
index f906e452f5..50c162ecee 100644
--- a/xen/arch/arm/setup_mpu.c
+++ b/xen/arch/arm/setup_mpu.c
@@ -106,6 +106,15 @@ bool __init check_boot_module(bootmodule_kind kind,
     return false;
 }
 
+static void __init discard_initial_modules_one(void* data)
+{
+    unsigned int i;
+
+    for_each_set_bit( i, (const unsigned long *)&initial_module_mask,
+                      MAX_MPU_PROTECTION_REGIONS )
+        disable_mpu_region_from_index(i);
+}
+
 void __init discard_initial_modules(void)
 {
     unsigned int i = 0;
@@ -131,6 +140,8 @@ void __init discard_initial_modules(void)
                   start, end);
         set_bit(rc, initial_module_mask);
     }
+
+    smp_call_function(discard_initial_modules_one, NULL, 1);
 }
 
 void __init setup_mm(void)
-- 
2.25.1

