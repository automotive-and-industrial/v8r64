From 358f7450dd96620afb12f2dbb657903bdcee1418 Mon Sep 17 00:00:00 2001
From: Wei Chen <wei.chen@arm.com>
Date: Sat, 21 May 2022 00:56:39 +0800
Subject: [PATCH 34/47] xen/arm: add Kconfig option to enable Armv8-R AArch64
 support

Introduce a Kconfig option to enable Armv8-R64 architecture
support. ARM_SECURE_STATE and HAS_MPU will be selected by
ARM_V8R by default, because Armv8-R64 only has single secure
state and PMSAv8-64 on secure-EL2.

Change-Id: I4df7edbadc4d8a0eea4e3bcb42e722ae496f9c94
Issue-ID: SCM-4571
Signed-off-by: Wei Chen <wei.chen@arm.com>
Upstream-Status: Inappropriate [other]
  Implementation pending further discussion
Signed-off-by: Robbie Cao <robbie.cao@arm.com>
---
 xen/arch/arm/Kconfig              | 14 ++++++++++++++
 xen/arch/arm/include/asm/mm_mmu.h |  2 ++
 xen/arch/arm/include/asm/mm_mpu.h |  3 +++
 xen/arch/arm/mm_mpu.c             | 16 ++++++++++++++++
 xen/arch/arm/smpboot.c            |  4 ++--
 5 files changed, 37 insertions(+), 2 deletions(-)

diff --git a/xen/arch/arm/Kconfig b/xen/arch/arm/Kconfig
index 3571f96cb8..8d2032ef73 100644
--- a/xen/arch/arm/Kconfig
+++ b/xen/arch/arm/Kconfig
@@ -42,6 +42,17 @@ config ARM_EFI
 config ARM_SECURE_STATE
 	bool
 
+config ARM_V8R
+	bool "ARMv8-R AArch64 architecture support (UNSUPPORTED)" if UNSUPPORTED
+	default n
+	select HAS_MPU
+	select ARM_SECURE_STATE
+        select STATIC_MEMORY
+	depends on ARM_64
+	help
+	  This option enables Armv8-R profile for Arm64. Enabling this option
+	  results in selecting MPU.
+
 config GICV3
 	bool "GICv3 driver"
 	depends on ARM_64 && !NEW_VGIC
@@ -55,6 +66,9 @@ config HAS_ITS
         bool "GICv3 ITS MSI controller support (UNSUPPORTED)" if UNSUPPORTED
         depends on GICV3 && !NEW_VGIC
 
+config HAS_MPU
+	bool
+
 config HVM
         def_bool y
 
diff --git a/xen/arch/arm/include/asm/mm_mmu.h b/xen/arch/arm/include/asm/mm_mmu.h
index 44c9e96210..f59c197936 100644
--- a/xen/arch/arm/include/asm/mm_mmu.h
+++ b/xen/arch/arm/include/asm/mm_mmu.h
@@ -14,6 +14,8 @@ extern void mmu_init_secondary_cpu(void);
 extern uint64_t init_ttbr;
 
 #define update_mm() do {} while ( 0 )
+#define INIT_SECONDARY_MM_DATA(cpu) init_secondary_pagetables(cpu)
+#define MM_INIT_SECONDARY_CPU() mmu_init_secondary_cpu()
 
 static inline paddr_t __virt_to_maddr(vaddr_t va)
 {
diff --git a/xen/arch/arm/include/asm/mm_mpu.h b/xen/arch/arm/include/asm/mm_mpu.h
index 893ce858b5..f28d0317a1 100644
--- a/xen/arch/arm/include/asm/mm_mpu.h
+++ b/xen/arch/arm/include/asm/mm_mpu.h
@@ -64,6 +64,9 @@ uint8_t load_mpu_supported_region_el1(void);
 void save_el1_mpu_regions(pr_t *pr);
 void restore_el1_mpu_regions(pr_t *pr);
 
+#define INIT_SECONDARY_MM_DATA(cpu) (0)
+#define MM_INIT_SECONDARY_CPU() do {} while ( 0 )
+
 static inline paddr_t __virt_to_maddr(vaddr_t va)
 {
     return (paddr_t)va;
diff --git a/xen/arch/arm/mm_mpu.c b/xen/arch/arm/mm_mpu.c
index 61ff4aec5c..8cbeeb85e3 100644
--- a/xen/arch/arm/mm_mpu.c
+++ b/xen/arch/arm/mm_mpu.c
@@ -831,6 +831,22 @@ int reorder_xen_mpumap(void)
     return 0;
 }
 
+int xenmem_add_to_physmap_one(struct domain *d, unsigned int space,
+                              union add_to_physmap_extra extra,
+                              unsigned long idx, gfn_t gfn)
+{
+    return 0;
+}
+
+long arch_memory_op(int op, XEN_GUEST_HANDLE_PARAM(void) arg)
+{
+    return -ENOSYS;
+}
+
+void dump_hyp_walk(vaddr_t addr)
+{
+}
+
 void * __init early_fdt_map(paddr_t fdt_paddr)
 {
     void *fdt_virt;
diff --git a/xen/arch/arm/smpboot.c b/xen/arch/arm/smpboot.c
index f7bda3a18b..92d35de535 100644
--- a/xen/arch/arm/smpboot.c
+++ b/xen/arch/arm/smpboot.c
@@ -368,7 +368,7 @@ void start_secondary(void)
      */
     update_system_features(&current_cpu_data);
 
-    mmu_init_secondary_cpu();
+    MM_INIT_SECONDARY_CPU();
 
     gic_init_secondary_cpu();
 
@@ -457,7 +457,7 @@ int __cpu_up(unsigned int cpu)
 
     printk("Bringing up CPU%d\n", cpu);
 
-    rc = init_secondary_pagetables(cpu);
+    rc = INIT_SECONDARY_MM_DATA(cpu);
     if ( rc < 0 )
         return rc;
 
-- 
2.25.1

