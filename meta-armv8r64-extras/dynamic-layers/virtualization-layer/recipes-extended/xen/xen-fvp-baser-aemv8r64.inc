# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

COMPATIBLE_MACHINE = "fvp-baser-aemv8r64"

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
SRC_URI:append = " \
    file://fvp-baser-aemv8r64.cfg \
    file://0001-xen-arm-remove-xen_phys_start-and-xenheap_phys_end-f.patch \
    file://0002-xen-arm-disable-EFI-boot-services-for-MPU-systems.patch \
    file://0003-xen-arm-adjust-Xen-TLB-helpers-for-Armv8-R64-PMSA.patch \
    file://0004-xen-arm-define-Xen-start-address-for-FVP-BaseR-platf.patch \
    file://0005-xen-arm-split-MMU-and-MPU-config-files-from-config.h.patch \
    file://0006-xen-arm-implement-FIXMAP_ADDR-for-MPU-systems.patch \
    file://0007-xen-arm64-move-MMU-related-code-from-head.S-to-head_.patch \
    file://0008-xen-arm64-create-boot-time-MPU-protection-regions.patch \
    file://0009-xen-arm64-introduce-helpers-for-MPU-enable-disable.patch \
    file://0010-xen-arm64-add-setup_fixmap-and-remove_identity_mappi.patch \
    file://0011-xen-arm-implement-stubs-of-virt-maddr-convertion-for.patch \
    file://0012-xen-arm-create-boot-time-MPU-configuration-for-Xen-s.patch \
    file://0013-xen-arm-write-boot-time-MPU-configuration-to-MPU-reg.patch \
    file://0014-xen-arm-implement-early_fdt_map-for-MPU-systems.patch \
    file://0015-xen-arm-initialize-Xen-heap-for-MPU-system.patch \
    file://0016-xen-arm-initialize-frametable-for-MPU-system.patch \
    file://0017-xen-arm-implement-setup_mm-for-MPU-system.patch \
    file://0018-xen-arm-relocate-MPU-configuration-map-from-heap.patch \
    file://0019-xen-arm-map-device-memory-section-to-MPU-protection-.patch \
    file://0020-xen-arm-implement-ioremap_attr-for-MPU-system.patch \
    file://0021-xen-arm-disable-VMAP-sub-system-for-MPU-systems.patch \
    file://0022-xen-arm-map-boot-module-section-to-MPU-protection-re.patch \
    file://0023-xen-arm-free-init-memory-in-MPU-system.patch \
    file://0024-xen-arm-Use-secure-hypervisor-timer-for-Armv8-R.patch \
    file://0025-xen-arm-Disable-GIC-two-security-state-and-Arm-SMMU-.patch \
    file://0026-xen-arm-introduce-arch_init_finialize-for-MPU-system.patch \
    file://0027-xen-arm-introduce-MPU-guest-memory-section.patch \
    file://0028-xen-arm-move-MMU-specific-P2M-code-to-p2m_mmu.c.patch \
    file://0029-xen-arm-implement-setup_virt_paging-for-MPU-system.patch \
    file://0030-xen-arm-implement-p2m_init-for-MPU-system.patch \
    file://0031-xen-arm-implement-p2m_get_entry-p2m_set_entry-for-MP.patch \
    file://0032-xen-arm-introduce-MPU-domain-for-MPU-systems.patch \
    file://0033-xen-arm-Add-context-switch-support-for-vCPU-of-MPU-d.patch \
    file://0034-xen-arm-add-Kconfig-option-to-enable-Armv8-R-AArch64.patch \
    file://0035-xen-arm-prepare-SMP-support-for-MPU-systems.patch \
    file://0036-xen-arm-add-firmware-based-PSCI-function-call-for-SM.patch \
    file://0037-xen-arm-dump-boot-MPU-configuration-for-debug-config.patch \
    file://0038-xen-mpu-enable-P2M-permission-p2m_map_foreign_rw-p2m.patch \
    file://0039-xen-mpu-introduce-new-mpu-section-mpu-shared-memory-.patch \
    file://0040-xen-mpu-map-shared-memory-on-boot-and-during-context.patch \
    file://0041-xen-mpu-enable-statically-configured-shared_info-on-.patch \
    file://0042-xen-mpu-disable-extended-regions-on-MPU-system.patch \
    file://0043-xen-arm-add-xen-dom0less-property-in-hypervisor-node.patch \
    file://0044-xen-arm-do-stage-1-translation-in-software-on-MPU-sy.patch \
    file://0045-xen-arm-map-unmap-peer-domain-guest-memory-on-contex.patch \
    file://0046-xen-arm-introduce-xen-shared-info-property-on-guest-.patch \
    file://0047-xen-mpu-enable-event-channel-interrupt-and-wallclock.patch \
    file://0048-xen-arm-set-VGICD_CTLR_DEFAULT-as-single-secure-stat.patch \
    "

# Remove SDL from config, which adds many dependencies that are not needed
PACKAGECONFIG:remove = "sdl"

# Since we are building a dom0less system, remove dependency on xen-tools
unset do_deploy[depends]
