# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

XEN_ARMV8R64_EXTRAS_REQUIRE ?= ""
XEN_ARMV8R64_EXTRAS_REQUIRE:fvp-baser-aemv8r64 = "xen-fvp-baser-aemv8r64.inc"
require ${XEN_ARMV8R64_EXTRAS_REQUIRE}
