# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# Remove qemu from RRECOMMENDS, which adds many dependencies that are not needed
RRECOMMENDS:${PN}:remove = " qemu"
