/*
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT
*/
/dts-v1/;

/ {
    #address-cells = <0x02>;
    #size-cells = <0x02>;

    gic {
        #interrupt-cells = <0x03>;
        interrupt-controller;
        phandle = <0x01>;
    };

    passthrough {
        compatible = "simple-bus";
        ranges;
        #address-cells = <0x02>;
        #size-cells = <0x02>;

        refclk24mhz {
            compatible = "fixed-clock";
            #clock-cells = <0x00>;
            clock-output-names = "uartclk";
            clock-frequency = <24000000>;
            phandle = <0x06>;
        };

        refclk100mhz {
            compatible = "fixed-clock";
            #clock-cells = <0x00>;
            clock-output-names = "apb_pclk";
            clock-frequency = <100000000>;
            phandle = <0x07>;
        };

        refclk1hz {
            compatible = "fixed-clock";
            #clock-cells = <0>;
            clock-frequency = <1>;
            clock-output-names = "refclk1hz";
            phandle = <0x08>;
        };

        uart@9c0b0000 {
            compatible = "arm,pl011", "arm,primecell";
            reg = <0x00 0x9c0b0000 0x00 0x10000>;
            interrupt-parent = <0x01>;
            interrupts = <0x00 0x07 0x04>;
            clock-names = "uartclk", "apb_pclk";
            clocks = <0x06 0x07>;
            xen,path = "/uart@9c0b0000";
            xen,reg = <0x00 0x9c0b0000 0x00 0x10000 0x00 0x9c0b0000>;
            xen,force-assign-without-iommu;
        };

        rtc@9c170000 {
            compatible = "arm,pl031", "arm,primecell";
            reg = <0x0 0x9c170000 0x0 0x1000>;
            interrupt-parent = <0x01>;
            interrupts = <0x0 4 0x4>;
            clocks = <0x08>;
            clock-names = "apb_pclk";
            xen,path = "/rtc@9c170000";
            xen,reg = <0x00 0x9c170000 0x00 0x10000 0x00 0x9c170000>;
            xen,force-assign-without-iommu;
        };

        wdt@9c0f0000 {
            compatible = "arm,sp805", "arm,primecell";
            reg = <0x0 0x9c0f0000 0x0 0x1000>;
            interrupt-parent = <0x01>;
            interrupts = <0x0 0 0x4>;
            clocks = <0x06 0x07>;
            clock-names = "wdog_clk", "apb_pclk";
            xen,path = "/wdt@9c0f0000";
            xen,reg = <0x00 0x9c0f0000 0x00 0x10000 0x00 0x9c0f0000>;
            xen,force-assign-without-iommu;
        };

        virtio-block@9c130000 {
            compatible = "virtio,mmio";
            reg = <0x00 0x9c130000 0x00 0x200>;
            interrupt-parent = <0x01>;
            interrupts = <0x00 42 0x04>;
            xen,path = "/virtio-block@9c130000";
            xen,reg = <0x0 0x9c130000 0x0 0x10000 0x0 0x9c130000>;
            xen,force-assign-without-iommu;
        };

        virtio-p9@9c140000 {
            compatible = "virtio,mmio";
            reg = <0x0 0x9c140000 0x0 0x200>;
            interrupt-parent = <0x01>;
            interrupts = <0x0 43 0x4>;
            xen,path = "/virtio-p9@9c140000";
            xen,reg = <0x0 0x9c140000 0x0 0x10000 0x0 0x9c140000>;
            xen,force-assign-without-iommu;
        };

        virtio-net@9c150000 {
            compatible = "virtio,mmio";
            reg = <0x00 0x9c150000 0x00 0x200>;
            interrupt-parent = <0x01>;
            interrupts = <0x00 44 0x04>;
            xen,path = "/virtio-net@9c150000";
            xen,reg = <0x0 0x9c150000 0x0 0x10000 0x0 0x9c150000>;
            xen,force-assign-without-iommu;
        };

        virtio-rng@9c200000 {
            compatible = "virtio,mmio";
            reg = <0 0x9c200000 0 0x200>;
            interrupt-parent = <0x01>;
            interrupts = <0x0 46 0x4>;
            xen,path = "/virtio-rng@9c200000";
            xen,reg = <0x0 0x9c200000 0x0 0x10000 0x0 0x9c200000>;
            xen,force-assign-without-iommu;
        };
    };
};
