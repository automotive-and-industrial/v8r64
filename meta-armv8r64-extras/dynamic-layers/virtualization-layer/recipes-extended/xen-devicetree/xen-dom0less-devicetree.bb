# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Xen device tree for dom0less"
DESCRIPTION = "Generates a Xen device tree overlay"

inherit deploy nopackages features_check xen_dom0less_devicetree

REQUIRED_DISTRO_FEATURES = "xen"
COMPATIBLE_MACHINE = "fvp-baser-aemv8r64"
PACKAGE_ARCH = "${MACHINE_ARCH}"

OVERLAY_TEMPLATE ?= "overlay.template.dts"
DOM_TEMPLATE ?= "dom.template.dtsi"
PASSTHROUGH_LINUX_TEMPLATE ?= "passthrough-linux.template.dts"
PASSTHROUGH_ZEPHYR_TEMPLATE ?= "passthrough-zephyr.template.dts"

SRC_URI = "\
    file://${OVERLAY_TEMPLATE} \
    file://${DOM_TEMPLATE} \
    file://${PASSTHROUGH_LINUX_TEMPLATE} \
    file://${PASSTHROUGH_ZEPHYR_TEMPLATE} \
    "

LICENSE = "MIT"
LIC_FILES_CHKSUM = "\
    file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302 \
    "
