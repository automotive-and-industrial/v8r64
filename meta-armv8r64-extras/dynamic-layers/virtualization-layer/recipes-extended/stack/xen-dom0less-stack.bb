# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Xen dom0less stack"
DESCRIPTION = "An helper recipe which builds the linked Linux image along with \
its dependencies"

LICENSE = "MIT"

inherit nopackages xen_dom0less_config features_check

REQUIRED_DISTRO_FEATURES = "xen"
COMPATIBLE_MACHINE = "fvp-baser-aemv8r64"
PACKAGE_ARCH = "${MACHINE_ARCH}"

do_fetch[noexec] = "1"
do_unpack[noexec] = "1"
do_patch[noexec] = "1"
do_configure[noexec] = "1"
do_compile[noexec] = "1"
do_install[noexec] = "1"
deltask do_populate_lic
deltask do_populate_sysroot

do_build[depends] += "${XEN_DOM0LESS_LINUX_IMAGE}:do_build"
