# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Recipe for the rpmsg-demo on Linux"
DESCRIPTION = "Sample program that demonstrates OpenAMP RPMsg on Linux."

LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/BSD-3-Clause;md5=550794465ba0ec5312d6919e203a55f9"

DEPENDS += "libmetal open-amp"
DEPENDS += "virtual/kernel xen-tools"
RDEPENDS:${PN} = "nginx-docker-demo"

# Here we specify the source directory, where we do all the building and
# expect sources to be placed
S = "${WORKDIR}/rpmsg-demo"

FILESEXTRAPATHS:prepend := "${V8R64_REPO_DIR}/components/apps:"
SRC_URI = " \
    file://rpmsg-demo \
    file://rpmsg-remote.init \
    "

inherit pkgconfig cmake \
    ${@oe.utils.conditional('XEN_DOM0LESS_ZEPHYR_APPLICATION', \
                            'zephyr-rpmsg-demo', 'update-rc.d', '', d)}

TARGET_OS = "linux"
TUNE_ARCH = "aarch64"
OPENAMP_MACHINE = "generic"

EXTRA_OECMAKE = " \
    -DLIB_INSTALL_DIR=${libdir} \
    -DLIBEXEC_INSTALL_DIR=${libexecdir} \
    -DMACHINE=${OPENAMP_MACHINE} \
    "

do_install:append() {
    if ${@bb.utils.contains('DISTRO_FEATURES', 'sysvinit', 'true', 'false', d)}; then
        install -d ${D}${sysconfdir}/init.d
        install -m 0755 ${WORKDIR}/rpmsg-remote.init ${D}${sysconfdir}/init.d/
    fi
}

INITSCRIPT_PACKAGES += "${@bb.utils.contains('DISTRO_FEATURES', 'sysvinit', '${PN}', '', d)}"
INITSCRIPT_NAME:${PN} = "${@bb.utils.contains('DISTRO_FEATURES', 'sysvinit', 'rpmsg-remote.init', '', d)}"
INITSCRIPT_PARAMS:${PN} = "${@oe.utils.conditional('XEN_DOM0LESS_DOM_LINUX_DEMO_AUTORUN', '1', \
                                                   'start 99 5 2 . stop 20 0 1 6 .', 'disable', d)}"
