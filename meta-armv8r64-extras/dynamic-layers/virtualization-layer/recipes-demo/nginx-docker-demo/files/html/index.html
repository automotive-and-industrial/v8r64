<!--
# Copyright (c) 2022, Arm Limited.
# SPDX-License-Identifier: MIT
-->
<!DOCTYPE html>
<html>
<head>
  <title>Test Page for Nginx on Armv8-R AArch64</title>
  <style>
    html {
      color-scheme: light dark;
    }
    body {
      width: 35em;
      margin: 0 auto;
      font-family: Tahoma, Verdana, Arial, sans-serif;
    }
  </style>
</head>

<body>
  <h1>Welcome to nginx on Armv8-R AArch64!</h1>
  <p>
    This page is used to demonstrate the proper operation of the
    <strong>nginx</strong> HTTP server on the Armv8-R AEM FVP (AArch64)
    platform. If you can read this page, it means that the web server is
    working properly.
  </p>
  <p>
    This web server runs in a docker container which is installed by the Armv8-R
    AArch64 software stack. For the Armv8-R AArch64 software stack, please refer
    to
    <a href="https://armv8r64-refstack.docs.arm.com/">
      armv8r64-refstack.docs.arm.com
    </a>.
  </p>
  <p>
    The Armv8-R AArch64 software stack runs on the Armv8-R AEM FVP, where FVP
    stands for Fixed Virtual Platforms, and AEM stands for Architecture Envelope
    Model. Please refer to
    <a href="https://developer.arm.com/docs/100966/latest">
      Arm Architecture Reference Manual
    </a>
    for more details.
  </p>
  <p>
    Armv8-R AARch64 is the latest R-Profile architecture that adds 64-bit
    execution capability and up to 48-bit physical addressing to the classic Arm
    real-time processor architecture. See
    <a href="https://developer.arm.com/documentation/ddi0600/ac">
      Fast Models Fixed Virtual Platforms (FVP) Reference Guide
    </a>
    for more information.
  </p>
  <p>
    For nginx documentation and support please refer to
    <a href="http://nginx.org/">nginx.org</a>.
  </p>
</body>
</html>
