#!/usr/bin/env bash

# Copyright (c) 2022, Arm Limited.
# SPDX-License-Identifier: MIT

if [ "$( docker ps -a -f name=nginx | wc -l )" -eq 2 ]; then
  echo "Nginx service is already running."
else
  echo "Starting nginx service."
  docker run -p 80:80 \
    --name nginx \
    -v /usr/share/nginx/html:/usr/share/nginx/html \
    --restart=on-failure \
    -d nginx:stable-alpine
fi
