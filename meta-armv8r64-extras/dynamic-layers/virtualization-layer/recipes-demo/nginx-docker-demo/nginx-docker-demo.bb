# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Files for nginx docker demo."
DESCRIPTION = "Install files for nginx docker."

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

RDEPENDS:${PN} = "docker"

SRC_URI = " \
    file://utils/run-nginx-docker.sh;subdir=src \
    file://html/index.html;subdir=src \
    file://html/zephyr-status.html;subdir=src \
    file://nginx-docker.init \
"

inherit ${@oe.utils.conditional('XEN_DOM0LESS_DOM_LINUX_DEMO_AUTORUN', '1', 'update-rc.d', '', d)}

S = "${WORKDIR}/src"
target_dir = "${datadir}/nginx"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
    install -d "${D}${target_dir}/"
    install -d "${D}${target_dir}/html/"
    install -d "${D}${target_dir}/utils/"
    install -m 644 "${S}/html/index.html" "${D}${target_dir}/html/"
    install -m 644 "${S}/html/zephyr-status.html" "${D}${target_dir}/html/"
    install -m 0755 "${S}/utils/run-nginx-docker.sh" "${D}${target_dir}/utils/"

    if ${@bb.utils.contains('DISTRO_FEATURES', 'sysvinit', 'true', 'false', d)}; then
        install -d ${D}${sysconfdir}/init.d
        install -m 0755 ${WORKDIR}/nginx-docker.init ${D}${sysconfdir}/init.d/
    fi
}

INITSCRIPT_PACKAGES += "${@bb.utils.contains('DISTRO_FEATURES', 'sysvinit', '${PN}', '', d)}"
INITSCRIPT_NAME:${PN} = "${@bb.utils.contains('DISTRO_FEATURES', 'sysvinit', 'nginx-docker.init', '', d)}"
INITSCRIPT_PARAMS:${PN} = "${@oe.utils.conditional('XEN_DOM0LESS_DOM_LINUX_DEMO_AUTORUN', '1', \
                                                   'start 99 5 2 . stop 20 0 1 6 .', 'disable', d)}"

FILES:${PN} += "${target_dir}/*"
