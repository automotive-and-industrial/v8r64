# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Test docker runtime on Armv8R AArch64 virtualization software stack"
DESCRIPTION = "A set of tests to check docker runtime using BATS"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    file://testcases;subdir=src \
    file://utils;subdir=src \
    file://run-ptest \
"

inherit ptest

COMPATIBLE_MACHINE = "fvp-baser-aemv8r64"
PACKAGE_ARCH = "${MACHINE_ARCH}"

S = "${WORKDIR}/src"
target_dir = "/opt/${PN}"

RDEPENDS:${PN} += " bats nginx-docker-demo"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
    install -d "${D}${target_dir}/"
    cp -r ${S}/testcases ${D}${target_dir}/
    cp -r ${S}/utils ${D}${target_dir}/
}

FILES:${PN} += "${target_dir}/*"
