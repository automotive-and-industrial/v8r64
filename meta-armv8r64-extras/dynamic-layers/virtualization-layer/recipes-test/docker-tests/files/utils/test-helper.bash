#!/usr/bin/env bash
#
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT
#

# Check if docker has been installed
# 0 for success
# 1 for error
check_docker_engine_installation() {
    if [[ -x "/usr/bin/containerd" ]] && [[ -x "/usr/bin/dockerd" ]] \
       && [[ -x "/usr/bin/docker" ]]; then
        return 0
    else
        return 1
    fi
}
