#!/usr/bin/env bats
#
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT
#

load ../utils/test-helper.bash

setup_file() {
    export BATS_TEST_TIMEOUT="1500"
}

setup() {
    run check_docker_engine_installation
    [ "$status" -eq 0 ]
    /usr/share/nginx/utils/run-nginx-docker.sh
    # Avoid the test case below start before the "docker run" ends
    docker ps
}

@test "Check nginx web service" {
    run wget http://127.0.0.1:80/zephyr-status.html -O /dev/null
    [ "$status" -eq 0 ]
}
