#!/usr/bin/env bats
#
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT
#

load ../utils/test-helper.bash

@test "Check docker runtime environment" {
    run check_docker_engine_installation
    [ "$status" -eq 0 ]
    run pgrep dockerd
    [ "$status" -eq 0 ]
    run docker ps
    [ "$status" -eq 0 ]
}
