# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "U-Boot boot script for Xen"
DESCRIPTION = "Generate a U-Boot boot script which automatically loads the \
binaries defined in the Xen domain configuration"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;\
md5=0835ade698e0bcf8506ecda2f7b4f302"
COMPATIBLE_MACHINE = "fvp-baser-aemv8r64"

SRC_URI = "file://boot.template.cmd"

DEPENDS = "u-boot-mkimage-native"

INHIBIT_DEFAULT_DEPS = "1"

BOOT_SCRIPT_TEMPLATE = "${WORKDIR}/boot.template.cmd"
BOOT_SCRIPT_SOURCE = "${WORKDIR}/boot.cmd"
BOOT_SCRIPT_BINARY ?= "boot.scr"

inherit xen_dom0less_config xen_dom0less_template kernel-arch deploy nopackages

python do_configure() {
    # Generate the list of files to load at runtime
    files = {"$kernel_addr_r": "xen"}
    for dom in d.getVar('XEN_DOM0LESS_DOMAINS').split(' '):
        binary_address = d.getVarFlag(dom, 'binary_address')
        files[binary_address] = d.getVarFlag(dom, 'binary')
        fdt_address = d.getVarFlag(dom, 'dtb_address')
        files[fdt_address] = dom0less_passthrough_dtb(dom, d)

    load_files = ""
    for (addr, path) in files.items():
        load_files += \
                f"load $devtype $devnum:$distro_bootpart {addr} {path}\n"

    # Process the boot script template
    boot_script_template = d.getVar('BOOT_SCRIPT_TEMPLATE')
    boot_script_output = d.getVar('BOOT_SCRIPT_SOURCE')
    process_template(boot_script_template, boot_script_output,
            load_files=load_files)
}

do_compile() {
    mkimage -A ${UBOOT_ARCH} -T script -C none -n 'Boot script' \
        -d ${BOOT_SCRIPT_SOURCE} ${BOOT_SCRIPT_BINARY}
}

do_install[noexec] = "1"

do_deploy() {
    install -d ${DEPLOYDIR}
    install -m 0644 ${BOOT_SCRIPT_BINARY} ${DEPLOYDIR}
}
addtask deploy after do_compile before do_build
