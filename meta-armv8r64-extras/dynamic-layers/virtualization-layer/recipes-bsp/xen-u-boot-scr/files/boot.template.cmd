# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# fdt_overlay_addr is only used temporarily, so process it first
# so that other files can use the same memory
setenv fdt_overlay_addr 0x10000000
setenv fdt_addr_r_size 0x00100000
load $$devtype $$devnum:$$distro_bootpart $$fdt_overlay_addr overlay.dtbo
fdt addr $$fdtcontroladdr
fdt move $$fdtcontroladdr $$fdt_addr_r $$fdt_addr_r_size
fdt addr $$fdt_addr_r
fdt apply $$fdt_overlay_addr
fdt resize

# Load the module files
${load_files}

# Disable switching to S-EL1
setenv armv8_switch_to_el1 n
# Boot Xen
booti $$kernel_addr_r - $$fdt_addr_r
