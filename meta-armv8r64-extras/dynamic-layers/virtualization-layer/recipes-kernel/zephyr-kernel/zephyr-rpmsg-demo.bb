# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "OpenAMP RPMsg demo"
DESCRIPTION = "A sample to demo OpenAMP RPMsg host in Zephyr"

require recipes-kernel/zephyr-kernel/zephyr-sample.inc

FILESEXTRAPATHS:prepend := "${V8R64_REPO_DIR}/components/apps:"
SRC_URI:append = " file://rpmsg-demo;subdir=git"

ZEPHYR_MODULES = "${S}/modules/hal/libmetal\;${S}/modules/lib/open-amp"
EXTRA_OECMAKE:append = " -DZEPHYR_MODULES=${ZEPHYR_MODULES}"

ZEPHYR_SRC_DIR = "${S}/rpmsg-demo/demos/zephyr/rpmsg-host"
