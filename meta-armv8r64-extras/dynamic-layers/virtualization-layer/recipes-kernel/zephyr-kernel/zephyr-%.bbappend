# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

ZEPHYR_XEN_ARMV8R64_EXTRAS_REQUIRE ?= ""
ZEPHYR_XEN_ARMV8R64_EXTRAS_REQUIRE:fvp-baser-aemv8r64 = "zephyr-fvp-baser-aemv8r64-xen.inc"

require ${@bb.utils.contains('DISTRO_FEATURES', 'xen', \
        d.getVar('ZEPHYR_XEN_ARMV8R64_EXTRAS_REQUIRE'), '', d)}
