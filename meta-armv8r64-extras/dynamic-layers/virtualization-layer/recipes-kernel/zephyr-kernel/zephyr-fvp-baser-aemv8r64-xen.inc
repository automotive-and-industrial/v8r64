# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
SRC_URI:append = "\
    file://fvp-baser-aemv8r64-xen.conf \
    file://fvp-baser-aemv8r64-xen.overlay \
    file://0001-driver-xen-add-config-XEN-and-XEN_DOMAIN_SHARED_MEM.patch;patchdir=zephyr \
    file://0002-soc-arm64-fvp_aemv8r-add-xen-domain-shared-memory.patch;patchdir=zephyr \
    file://0003-boards-fvp_baser_aemv8r-Add-hypervisor-node-in-devic.patch;patchdir=zephyr \
    file://0004-drivers-xen-save-events-comes-before-channel-is-boun.patch;patchdir=zephyr \
    "

OVERLAY_CONFIG ?= "${WORKDIR}/fvp-baser-aemv8r64-xen.conf"
DTC_OVERLAY_FILE ?= "${WORKDIR}/fvp-baser-aemv8r64-xen.overlay"

# This should match the Xen domain configuration
ZEPHYR_NUM_CPUS ?= "2"

EXTRA_OECMAKE:append = " \
    -DOVERLAY_CONFIG='${OVERLAY_CONFIG}' \
    -DDTC_OVERLAY_FILE='${DTC_OVERLAY_FILE}' \
    "
