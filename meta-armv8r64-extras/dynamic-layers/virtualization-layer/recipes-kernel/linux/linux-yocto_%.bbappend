# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend:fvp-baser-aemv8r64 := "${THISDIR}:${THISDIR}/files:"
SRC_URI:append:fvp-baser-aemv8r64 = " \
    file://kmeta-extra;type=kmeta;name=kmeta-extra;destsuffix=kmeta-extra \
    "

KERNEL_FEATURES:append:fvp-baser-aemv8r64 = " \
    ${@bb.utils.contains('DISTRO_FEATURES', \
                         'xen', \
                         'features/xen-shmem.scc', \
                         '', d)} \
    "
