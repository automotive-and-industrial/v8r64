#
# Copyright (c) 2022 Arm Limited
#
# SPDX-License-Identifier: MIT

# To install docker in the target image, add the following
# configuration in local.conf:
#
#     DISTRO_FEATURES:append = " virtualization"

RRECOMMENDS:${PN} += " kernel-module-xt-nat"
