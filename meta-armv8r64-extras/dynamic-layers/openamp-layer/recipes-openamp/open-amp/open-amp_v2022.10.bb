# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

LIC_FILES_CHKSUM = "file://LICENSE.md;md5=ab88daf995c0bd0071c2e1e55f3d3505"

SRCBRANCH ?= "main"
SRCREV = "v2022.10.0"
BRANCH = "v2022.10"
PV = "${SRCBRANCH}+git${SRCPV}"

require recipes-openamp/open-amp/open-amp.inc
