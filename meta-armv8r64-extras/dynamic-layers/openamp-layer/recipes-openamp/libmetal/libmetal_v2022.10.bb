# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

SRCBRANCH ?= "main"
SRCREV = "v2022.10.0"
BRANCH = "v2022.10"
PV = "${SRCBRANCH}+git${SRCPV}"

require recipes-openamp/libmetal/libmetal.inc
