#!/usr/bin/env bats
#
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT
#

load ../utils/test-helper.bash

@test "Check watchdog devices" {
    check_devices "watchdog" 1 "sp805-wdt"
}
