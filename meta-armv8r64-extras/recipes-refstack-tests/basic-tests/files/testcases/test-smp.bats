#!/usr/bin/env bats
#
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT
#


load ../utils/test-helper.bash

@test "Check SMP is enabled" {
    run smp_check_enable
    echo "status: ${status}"
    echo "output: ${output}"
    [ "${status}" -eq "0" ]
}

@test "Check SMP cores are up" {
    run smp_check_all_core_available
    echo "status: ${status}"
    echo "output: ${output}"
    [ "${status}" -eq "0" ]
}

@test "Check SMP CPU hot plug" {
    run smp_check_hotplug
    echo "status: ${status}"
    echo "output: ${output}"
    [ "${status}" -eq "0" ]
}

@test "Check SMP removing cores" {
    run smp_check_remove_all_cpu
    echo "status: ${status}"
    echo "output: ${output}"
    [ "${status}" -eq "0" ]
}
