#!/usr/bin/env bats
#
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT
#

load ../utils/test-helper.bash

@test "Check network physical device" {
    run network_check_physical_device_exist
    echo "status: ${status}"
    echo "output: ${output}"
    [ ${status} -eq "0" ]
}

@test "Check network IP address" {
    run network_check_physical_device_exist
    if [ ${status} -eq "1" ]; then
        skip "No physical device found"
    fi
    physical_nets=$(realpath /sys/class/net/* | grep -v 'virtual')
    run network_check_device_got_ip "${physical_nets[*]}"
    echo "status: ${status}"
    echo "output: ${output}"
    [ ${status} -eq "0" ]
}

@test "Check network connection" {
    check_devices "net" 2 "virtio_net|vif"

    # Check outbound network connections work
    run wget -O /dev/null "https://www.arm.com"
    echo "${output}"
    [ "${status}" -eq 0 ]
}
