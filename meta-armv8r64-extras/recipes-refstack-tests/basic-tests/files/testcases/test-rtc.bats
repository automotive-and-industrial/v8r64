#!/usr/bin/env bats
#
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT
#

load ../utils/test-helper.bash

@test "Check real-time clock devices" {
    check_devices "rtc" 1 "rtc-pl031"

    # Check hwclock runs successfully
    run hwclock
    echo "${output}"
    [ "$status" -eq 0 ]
}
