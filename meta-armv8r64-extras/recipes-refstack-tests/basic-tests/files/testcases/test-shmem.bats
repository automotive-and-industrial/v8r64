#!/usr/bin/env bats
#
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT
#

load ../utils/test-helper.bash

@test "Check Xen shmem device" {
    run check_domu

    if [[ "$status" -eq 0 ]]; then
        skip "System not running in a Xen domain"
    fi

    # Check if device node exists and is a character special file
    [ -c "/dev/xen_mem" ]
}
