#!/usr/bin/env bash
#
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT
#

# Determine if Linux running inside a Xen based virtual machine
# by checking file "/sys/hypervisor/uuid"
#
# Return value:
#     0: It does not exist -> Not related to XEN
#     1: It does exist, and is full of 0-s -> It is a XEN Dom0 or Dom0less
#     2: It does exist, and has a not-0 values -> It is a DomU
check_domu() {
    local uuid_path
    local uuid

    uuid_path="/sys/hypervisor/uuid"
    if [ -e "${uuid_path}" ]; then
        uuid=$(cat "${uuid_path}")
        if [[ ! $uuid =~ ^[0\-]+$ ]]; then
            return 2
        else
            return 1
        fi
    else
        return 0
    fi
}

check_devices() {
    local class
    local count
    local driver
    class=$1
    count=$2
    driver=$3

    # Find all the devices of $class and store in global array DEVICES
    mapfile -t DEVICES < <(find "/sys/class/$class" -type l -maxdepth 1)
    echo "devices: " "${DEVICES[@]}"

    # Assert that the number of DEVICES is greater than or equal to $count
    [ "${#DEVICES[@]}" -ge "$count" ]

    # Assert that at least one of DEVICES uses $driver
    local found
    for device in "${DEVICES[@]}"; do
        echo "device: ${device}"
        # Query the driver
        local device_driver
        device_driver=$(basename "$(readlink "${device}/device/driver")")
        echo "device_driver: ${device_driver}"
        if [[ "${device_driver}" =~ ${driver} ]]; then
            found=1
        fi
    done
    [ "${found}" ]
}

# Check if system has physical network device(s)
network_check_physical_device_exist() {
    if realpath /sys/class/net/* | grep -v 'virtual'; then
        return 0
    fi
    echo "No physical network device found"
    return 1
}

# Check if specific device(s) got ip normally
network_check_device_got_ip() {
    # $* is network device list
    # Example: 'network_check_device_got_ip eth0 eth1'
    local devices=$*
    local ret=0
    for device in ${devices}; do
        local dev
        dev=$(basename "${device}")
        if ip address show "${dev}" | grep 'inet'; then
            continue
        else
            echo "${dev} hasn't got ip address"
            ret=1
        fi
    done
    return ${ret}
}

# Check all CPU cores are up
smp_check_all_core_available() {
    local ret=0
    local fdt="/sys/firmware/devicetree/base"
    local fdt_core_num
    local running_core_num
    fdt_core_num=$(find "${fdt}/cpus" -maxdepth 1 -iname "*cpu@*" | wc -l)
    running_core_num=$(grep -c 'processor' /proc/cpuinfo)
    if [ "${fdt_core_num}" -ne "${running_core_num}" ]; then
        echo "Get ${fdt_core_num} core(s) in devicetree"
        echo "Get ${running_core_num} core(s) in /proc/cpuinfo"
        ret=1
    fi
    return ${ret}
}

# Check if system has more than one processor
smp_check_enable() {
    local ret=0
    local running_core_num
    running_core_num=$(grep -c 'processor' /proc/cpuinfo)
    if [ "${running_core_num}" -eq "1" ]; then
        echo "Only 1 core is running"
        ret=1
    fi
    return ${ret}
}

smp_turn_off_core() {
    local n=$1
    echo "0" > "/sys/devices/system/cpu/cpu${n}/online"
}

smp_turn_on_core() {
    local n=$1
    echo "1" > "/sys/devices/system/cpu/cpu${n}/online"
}

smp_is_core_online() {
    local n=$1
    grep "processor.*${n}" /proc/cpuinfo
}

smp_bring_up_all_core() {
    local cpus
    cpus=$(find /sys/devices/system/cpu -maxdepth 1 -type d -iname "*cpu[0-9]*" | grep -E -o '[0-9]+')
    for n in ${cpus}; do
        smp_turn_on_core "${n}"
    done
}

# Check CPU hot plug
smp_check_hotplug() {
    local cpus
    cpus=$(find /sys/devices/system/cpu -maxdepth 1 -type d -iname "*cpu[0-9]*" | grep -E -o '[0-9]+')
    for n in ${cpus}; do
        smp_turn_off_core "${n}"
        if smp_is_core_online "${n}"; then
            echo "turn off cpu${n} failed"
            return 1
        fi

        smp_turn_on_core "${n}"
        if ! smp_is_core_online "${n}"; then
            echo "turn on cpu${n} failed"
            return 1
        fi
    done
    return 0
}

# Check removing all cpus
# Kernel will raise a failure when we try to remove the last core
smp_check_remove_all_cpu() {
    local cpus
    cpus=$(find /sys/devices/system/cpu -maxdepth 1 -type d -iname "*cpu[0-9]*" | grep -E -o '[0-9]+')
    local ret=0
    for n in ${cpus}; do
        smp_turn_off_core "${n}"
        ret=$?
        if [ ${ret} -ne "0" ]; then
            break
        fi
    done

    local num_online
    num_online=$(grep -c 'processor' /proc/cpuinfo)
    if [ "${num_online}" -eq "1" ]; then
        ret=0
    else
        echo "Two or more cores are online"
        ret=1
    fi
    smp_bring_up_all_core
    return ${ret}
}
