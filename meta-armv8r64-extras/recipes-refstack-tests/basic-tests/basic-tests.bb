# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Basic test for Armv8R AArch64 reference stack"
DESCRIPTION = "This is a set of tests for Armv8R AArch64 refstack using BATS"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI = " \
    file://utils;subdir=src \
    file://testcases;subdir=src \
    file://run-ptest \
"

inherit ptest

COMPATIBLE_MACHINE = "fvp-baser-aemv8r64"
PACKAGE_ARCH = "${MACHINE_ARCH}"

S = "${WORKDIR}/src"
target_dir = "/opt/${PN}"

RDEPENDS:${PN} += " bats"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install() {
    install -d "${D}${target_dir}/"
    cp -r ${S}/testcases ${D}${target_dir}/
    cp -r ${S}/utils ${D}${target_dir}/
}

FILES:${PN} += "${target_dir}/*"
