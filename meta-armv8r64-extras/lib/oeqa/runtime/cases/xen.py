# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

from oeqa.runtime.case import OERuntimeTestCase


class XenBoot(OERuntimeTestCase):
    terminal = 'xen'

    def test_boot(self):
        self.target.expect(self.terminal, 'Starting kernel ...')
        self.target.expect(self.terminal, '\\(XEN\\) Brought up \\d+ CPUs')
