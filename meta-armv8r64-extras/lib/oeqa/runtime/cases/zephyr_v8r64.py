# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

from oeqa.runtime.case import OERuntimeTestCase


class ZephyrTests(OERuntimeTestCase):
    def has_xen(self):
        return 'xen' in self.td['DISTRO_FEATURES'].split()

    def setUp(self):
        self.console = 'zephyr_v8r64' if self.has_xen() else 'default'

    def check_boot(self):
        self.target.expect(self.console, 'Booting Zephyr OS build zephyr-')

    def check_application(self, application):
        if self.has_xen():
            if self.td['XEN_DOM0LESS_ZEPHYR_APPLICATION'] != application:
                self.skipTest(f'Skipping - not {application}')
        elif self.td['IMAGE_BASENAME'] != application:
            self.skipTest(f'Skipping - not {application}')

    def test_helloworld(self):
        self.check_application('zephyr-helloworld')
        self.check_boot()
        self.target.expect(self.console, 'Hello World!')

    def test_synchronization(self):
        self.check_application('zephyr-synchronization')
        self.check_boot()
        for _ in range(0, 10):
            self.target.expect(self.console,
                               'thread_a: Hello World from cpu 0 on .*!')
            self.target.expect(self.console,
                               'thread_b: Hello World from cpu 1 on .*!')

    def test_philosophers(self):
        self.check_application('zephyr-philosophers')
        self.check_boot()
        for _ in range(0, 10):
            self.target.expect(self.console, 'Philosopher 0 \\[.: 3\\]')
            self.target.expect(self.console, 'Philosopher 1 \\[.: 2\\]')
            self.target.expect(self.console, 'Philosopher 2 \\[.: 1\\]')
            self.target.expect(self.console, 'Philosopher 3 \\[.: 0\\]')
            self.target.expect(self.console, 'Philosopher 4 \\[.:-1\\]')
            self.target.expect(self.console, 'Philosopher 5 \\[.:-2\\]')
