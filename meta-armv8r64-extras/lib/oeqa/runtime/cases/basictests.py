# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

import re
from os import path, makedirs
from shutil import rmtree
from oeqa.runtime.case import OERuntimeTestCase
from oeqa.core.decorator.depends import OETestDepends
from oeqa.runtime.decorator.package import OEHasPackage

"""
The test module belongs to 'oeqa/runtime' tests including BasicTests test
class.

Usage:
    1. Add 'v8r64/meta-armv8r64-extras' layer to Yocto.
    2. Enable runtime tests on your machine.
    3. Append 'basictests' to 'TEST_SUITES' variable in 'local.conf'.
    4. Run runtime tests on your machine.

Please refer to: performing-automated-runtime-testing[1] for more information.

[1]: https://docs.yoctoproject.org/dev-manual/common-tasks.html#performing-automated-runtime-testing # nopep8
"""


class BasicTests(OERuntimeTestCase):
    """
    Basic Acceptance Test
    This test class is used to execute 'basic-tests' in Yocto runtime testing.
    """

    @OETestDepends(['linuxboot.LinuxBootTest.test_linux_boot'])
    @OEHasPackage(['basic-tests'])
    def test_basic_tests(self):
        cmd = 'bats /opt/basic-tests/testcases'
        status, output = self.target.run(cmd)
        msg = "basic-tests failed\n"
        msg += "-"*72 + '\n'
        msg += output + '\n'
        msg += "-"*72 + '\n'
        self.assertEqual(status, 0, msg=msg)
