# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

from oeqa.runtime.case import OERuntimeTestCase
from oeqa.core.decorator.depends import OETestDepends
from oeqa.runtime.decorator.package import OEHasPackage


class PtestDockerTests(OERuntimeTestCase):
    @OETestDepends(['linuxlogin.LinuxLoginTest.test_linux_login'])
    @OEHasPackage(['ptest-runner'])
    def test_ptestdocker(self):
        console = self.target.DEFAULT_CONSOLE

        self.target.sendline(console, 'ptest-runner docker-tests')
        self.target.expect(console, r'START: ptest-runner')
        self.logger.info('ptest-runner started')
        self.target.expect(console, r'TOTAL: (\d+) FAIL: (\d+)',
                           timeout=30*60)

        matches = self.target.match(console)
        self.assertGreater(int(matches[1]), 0,
                           msg='No tests have run')
        self.assertEqual(int(matches[2]), 0,
                         msg='At least one test has failed')

        self.target.expect(console, 'root@.*\\:~#', timeout=300)
