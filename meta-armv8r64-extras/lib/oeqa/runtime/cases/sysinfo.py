# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

from oeqa.runtime.case import OERuntimeTestCase
from oeqa.core.decorator.depends import OETestDepends

"""
The test module belongs to 'oeqa/runtime' tests including SysInfo test class.

Usage:
    1. Add 'v8r64/meta-armv8r64-extras' layer to Yocto.
    2. Enable runtime tests on your machine.
    3. Append 'sysinfo' to 'TEST_SUITES' variable in 'local.conf'.
    4. Run runtime tests on your machine.

Refer to: performing-automated-runtime-testing[1] for more information.

[1]: https://docs.yoctoproject.org/dev-manual/common-tasks.html#performing-automated-runtime-testing # nopep8
"""


class SysInfo(OERuntimeTestCase):

    """
    SysInfo test class
    Checking system information is the same between Yocto build system and
    Yocto image.
    """

    kernel_version_suffix = {
        'linux-yocto': 'standard',
        'linux-yocto-rt': 'preempt-rt',
        'linux-yocto-tiny': 'tiny',
    }

    @OETestDepends(['linuxboot.LinuxBootTest.test_linux_boot'])
    def test_hostname(self):
        cmd = 'cat /etc/hostname'
        status, output = self.target.run(cmd)
        self.assertEqual(status, 0, msg=f"Failed to execute {cmd}")

        machine = self.td.get('MACHINE', '')
        msg = f"Expected hostname is '{machine}', but got '{output}'"
        self.assertEqual(output, machine, msg=msg)

    @OETestDepends(['linuxboot.LinuxBootTest.test_linux_boot'])
    def test_kernel_version(self):
        kernel_type = self.td.get('PREFERRED_PROVIDER_virtual/kernel', '')
        kernel_version = self.td.get(
            f'PREFERRED_VERSION_{kernel_type}', '').replace('%', '')
        suffix = self.kernel_version_suffix[kernel_type]

        cmd = 'uname -r'
        status, output = self.target.run(cmd)
        self.assertEqual(status, 0, msg=f"Failed to execute {cmd}")

        msg = f"Expected prefix is {kernel_version}, but got {output}"
        self.assertTrue(kernel_version in output, msg=msg)

        msg = f"Expected suffix is {suffix}, but got {output}"
        self.assertTrue(suffix in output, msg=msg)
