# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

from oeqa.runtime.case import OERuntimeTestCase
from oeqa.core.decorator.depends import OETestDepends


class RpmsgTests(OERuntimeTestCase):
    zephyr_console = 'zephyr_v8r64'
    linux_console = 'default'

    def test_zephyr_boot(self):
        if (('XEN_DOM0LESS_DOM_ZEPHYR' not in
             self.td['XEN_DOM0LESS_DOMAINS'].split()) or
            (self.td['XEN_DOM0LESS_ZEPHYR_APPLICATION'] !=
             'zephyr-rpmsg-demo')):
            self.skipTest('Skipping - no zephyr domain')
        self.target.expect(self.zephyr_console,
                           r'Booting Zephyr OS build zephyr-',
                           timeout=30)

    @OETestDepends([
        'rpmsg.RpmsgTests.test_zephyr_boot',
        'linuxlogin.LinuxLoginTest.test_linux_login'
        ])
    def test_rpmsg_demo(self):
        self.target.sendline(self.linux_console, 'rpmsg-remote -t')
        for i in range(100):
            self.target.expect(self.zephyr_console, r'Received message: '
                               r'echo - \d+', timeout=120)
            self.target.expect(self.linux_console, r'Received message: '
                               r'ping - \d+', timeout=120)
        for i in range(10):
            self.target.expect(self.zephyr_console, (
                               r'Sent payload: idx - \d+, uptime - \d+, '
                               r'cycles - \d+, thread: total - \d+, '
                               r'peak - \d+, avg - \d+'),
                               timeout=120)
            self.target.expect(self.linux_console, (
                               r'Recv payload: idx - \d+, uptime - \d+, '
                               r'cycles - \d+, thread: total - \d+, '
                               r'peak - \d+, avg - \d+'),
                               timeout=120)

        self.target.expect(self.linux_console, 'root@.*\\:~#', timeout=300)
