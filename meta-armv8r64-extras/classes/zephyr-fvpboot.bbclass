# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# This class acts as an adapter between meta-arm's fvpboot and Zephyr apps
# Add this class to ZEPHYR_INHERIT_CLASSES to enable booting a Zephyr app using
# runfvp

inherit fvpboot

IMGDEPLOYDIR = "${DEPLOY_DIR_IMAGE}"
FVP_ZEPHYR_IMAGE ?= "${DEPLOY_DIR_IMAGE}/${PN}.elf"

python() {
    if not d.getVar('FVP_PROVIDER'):
        raise bb.parse.SkipRecipe('FVP_PROVIDER must be set.')
}
do_build[depends] += "${FVP_PROVIDER}:do_populate_sysroot"

addtask do_write_fvpboot_conf before do_build after do_deploy

FVP_EXTRA_ARGS = "-a ${FVP_ZEPHYR_IMAGE}"
FVP_CONFIG[bp.virtioblockdevice.image_path] = ""
FVP_CONFIG[bp.refcounter.use_real_time] = "0"
FVP_CONFIG[cluster0.NUM_CORES] = "${ZEPHYR_NUM_CPUS}"
