# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

inherit testimage

TEST_SUITES:append = " sysinfo basictests"
IMAGE_INSTALL:append = " openssh-sshd basic-tests-ptest"
DISTRO_FEATURES:append = " ptest"
