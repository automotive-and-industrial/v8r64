# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# Functionality to generate an image for Xen dom0less

inherit features_check fvpboot xen_dom0less_config

COMPATIBLE_MACHINE = "fvp-baser-aemv8r64"
REQUIRED_DISTRO_FEATURES:append = " xen"
IMAGE_LINK_NAME = "xen-dom0less-stack-${MACHINE}"

IMAGE_INSTALL:append = " nginx-docker-demo rpmsg-demo"

# Xen does not currently support this option
FVP_CONFIG[bp.refcounter.use_real_time] = "0"
# Map port 8022=22 for ssh, and 8080=80 for nginx web service
FVP_CONFIG[bp.virtio_net.hostbridge.userNetPorts] = "8022=22,8080=80"

WKS_FILE = "xen-image-dom0less.wks"
IMAGE_BOOT_FILES += "\
    boot.scr \
    xen-${MACHINE};xen \
    ${XEN_OVERLAY_DTBO_FILE};overlay.dtbo \
    "
do_image_wic[depends] += "\
    xen-u-boot-scr:do_deploy \
    xen:do_deploy \
    xen-dom0less-devicetree:do_deploy \
    "

# Dynamically append to IMAGE_BOOT_FILES and do_image_wic[depends] using
# XEN_DOM0LESS_DOMAINS
python() {
    files = []
    deps = []
    for dom in d.getVar('XEN_DOM0LESS_DOMAINS').split(' '):
        files += [
            d.getVarFlag(dom, 'binary'),
            dom0less_passthrough_dtb(dom, d)
        ]
        deps.append(d.getVarFlag(dom, 'depends'))

    d.appendVar('IMAGE_BOOT_FILES', ' ' + ' '.join(files))
    d.appendVarFlag('do_image_wic', 'depends', ' ' + ' '.join(deps))
}
