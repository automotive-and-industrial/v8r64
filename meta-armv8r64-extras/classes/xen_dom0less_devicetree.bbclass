# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# Functionality to generate an overlay dtbo and passthrough dtbs for the Xen
# dom0less stack

inherit xen_dom0less_config xen_dom0less_template

OVERLAY_TEMPLATE ??= ""
DOM_TEMPLATE ??= ""

DEPENDS += "dtc-native"

do_configure[noexec] = "1"
do_install[noexec] = "1"

# Ensure we can read the size of each domain's binary in do_compile
python() {
    for dom in d.getVar('XEN_DOM0LESS_DOMAINS').split(' '):
        d.appendVarFlag('do_compile', 'depends', ' ' +
                        d.getVarFlag(dom, 'depends'))
}

python xen_dom0less_devicetree_do_compile() {
    for dom in d.getVar('XEN_DOM0LESS_DOMAINS').split(' '):
        input_file = d.getVarFlag(dom, 'passthrough_template')
        input_path = os.path.join(d.getVar('WORKDIR'), input_file)
        processed_file = f'{dom.lower()}_passthrough.dts'
        params = d.getVarFlags(dom)
        process_template(input_path, processed_file, **params)
        run_dtc(processed_file, dom0less_passthrough_dtb(dom, d))

    input_path = os.path.join(d.getVar('WORKDIR'),
                              d.getVar('OVERLAY_TEMPLATE'))
    processed_file = 'overlay.dts'
    params = xen_template_params(d)
    process_template(input_path, processed_file, **params)
    processed_file_pre = processed_file + '.pre'
    run_cpp(processed_file, processed_file_pre)
    run_dtc(processed_file_pre, d.getVar('XEN_OVERLAY_DTBO_FILE'))
}

python xen_dom0less_devicetree_do_deploy() {
    import shutil

    for dom in d.getVar('XEN_DOM0LESS_DOMAINS').split(' '):
        shutil.copy(os.path.join(d.getVar('B'),
                    dom0less_passthrough_dtb(dom, d)),
                    d.getVar('DEPLOYDIR'))

    shutil.copy(os.path.join(d.getVar('B'),
                d.getVar('XEN_OVERLAY_DTBO_FILE')),
                d.getVar('DEPLOYDIR'))
}

addtask do_deploy after do_compile before do_build

EXPORT_FUNCTIONS do_compile do_deploy


def run_cpp(input_file, out_file):
    import subprocess
    args = ['cpp', '-nostdinc', '-undef', '-x', 'assembler-with-cpp',
            input_file, '-o', out_file]
    subprocess.run(args, check=True)


def run_dtc(input_file, out_file):
    import subprocess
    args = ['dtc', '-o', out_file, input_file]
    subprocess.run(args, check=True)


def xen_template_params(d):
    params = d.getVarFlags('XEN_DOM0LESS_PARAMS')
    params['doms'] = ''.join(generate_dom_node(dom, d)
                             for dom in d.getVar('XEN_DOM0LESS_DOMAINS')
                             .split(' '))
    return params


xen_template_params[vardeps] = "XEN_DOM0LESS_PARAMS"


def generate_dom_node(dom, d):
    binary_path = os.path.join(d.getVar('DEPLOY_DIR_IMAGE'),
                               d.getVarFlag(dom, 'binary'))
    binary_size = os.path.getsize(binary_path)
    dtb_size = os.path.getsize(dom0less_passthrough_dtb(dom, d))
    name = dom.lower()
    evtchn_enabled = d.getVar('XEN_DOM0LESS_EVTCHN_ENABLED')

    # Set default values
    params = {
        'name': name,
        'extra': '',
        'binary_size': binary_size,
        'dtb_size': dtb_size,
        'ram_size': hex(int(d.getVarFlag(dom, 'memory'), 0) * 1024),
        'bootargs': '',
        'cpus': '1',
        'evtchn_enabled': evtchn_enabled,
    }
    # Merge with dom's varflags
    params.update(d.getVarFlags(dom, expand=['binary']))

    input_path = os.path.join(d.getVar('WORKDIR'), d.getVar('DOM_TEMPLATE'))
    processed_file = f'dom-{name}.dtsi'
    process_template(input_path, processed_file, **params)
    with open(processed_file, 'r') as f:
        return f.read()
