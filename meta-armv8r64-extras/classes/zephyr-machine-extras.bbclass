# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

include dynamic-layers/zephyrcore/recipes-kernel/zephyr-kernel/zephyr-${MACHINE}.inc
