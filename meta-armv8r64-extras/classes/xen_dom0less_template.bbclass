# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# Templating for the Xen dom0less stack

# Call process_template from parent to parse template_file into out_file,
# replacing any ${variables} with params
def process_template(template_file, out_file, **params):
    from string import Template

    if not os.path.isfile(template_file):
        raise bb.parse.SkipRecipe(
                f"Template file {template_file} does not "
                "exist. Maybe you forgot to add the file to SRC_URI?")

    with open(template_file, 'r') as tf:
        template = Template(tf.read())
        with open(out_file, 'w') as f:
            f.write(template.substitute(**params))
