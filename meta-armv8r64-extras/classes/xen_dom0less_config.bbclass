# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# This bbclass sets some variables to be used across multiple recipes for Xen
# dom0less systems.

XEN_DOM0LESS_DTB_PREFIX = "${@os.path.splitext(os.path.basename(\
    d.getVar('KERNEL_DEVICETREE')))[0] + '-'}"
XEN_OVERLAY_DTBO_FILE = "${XEN_DOM0LESS_DTB_PREFIX}xen.dtbo"

# This defines the bootargs that will be passed to Xen
XEN_DOM0LESS_PARAMS[bootargs] ?= "console=dtuart dtuart=serial0 sched=null"

# These variables define Xen MPU region blocks. Guest-specific memory regions
# must fall within these global memory regions
XEN_DOM0LESS_PARAMS[boot_module_address] ?= "0x10000000"
XEN_DOM0LESS_PARAMS[boot_module_size] ?= "0x10000000"
XEN_DOM0LESS_PARAMS[guest_memory_address] ?= "0x20000000"
XEN_DOM0LESS_PARAMS[guest_memory_size] ?= "0x30000000"
# Xen's static heap
XEN_DOM0LESS_PARAMS[ram_address] ?= "0x50000000"
XEN_DOM0LESS_PARAMS[ram_size] ?= "0x20000000"
# The device memory
XEN_DOM0LESS_PARAMS[device_memory_address] ?= "0x80000000"
XEN_DOM0LESS_PARAMS[device_memory_size] ?= "0x80000000"
# The shared memeory
XEN_DOM0LESS_PARAMS[shared_memory_address] ?= "0x70000000"
XEN_DOM0LESS_PARAMS[shared_memory_size] ?= "0x1000000"


def dom0less_passthrough_dtb(dom, d):
    """
    This function takes a dom variable name and generates the name of the
    passthrough dtb file. The passthrough dtb is generated in
    xen-devicetree-dom0less.bb and used in xen-image-dom0less.bb
    """
    return d.getVar('XEN_DOM0LESS_DTB_PREFIX', True) + dom.lower() + \
        '_passthrough.dtb'


# Define the Xen domain configuration
#
# XEN_DOM0LESS_DOMAINS contains a list of domain-specific Bitbake variable.
# Each domain is configured using the varflags of each domain-specific Bitbake
# variable.
#
# The following varflags are used:
# binary: The name of the image file inside DEPLOY_DIR_IMAGE to include.
# binary_address: The address at which to load the binary.
# dtb_address: The address at which to load the passthrough device tree.
# passthrough_template: The filename of the template to use.
# ram_address: The address to use as static RAM for the domain.
# memory: The amount of RAM to allocate for the domain (in KB).
# bootargs: The bootargs to pass to the domain. Only valid for Linux.
# cpus: The number of vCPUs to assign to the domain.
# shared_info_addr: The address of the shared info for event channel.
# shared_mem_name: The name of the shared memory node for the domain.
# shared_mem_role: The role of the shared memory node for the domain.
# shared_memory_address: The start address of shared memory region
# shared_memory_size: The size of shared memory region.
# evtchn_name: The name of the event channel node in the domain.
# evtchn_link_to: The name of the event channel which is linked to.
# evtchn_port: The port number of the event channel in the domain.
# extra: This string will be added to the domain definition verbatim.

# These varflags must be defined for each domain. Other varflags are optional
# or have default values (see xen_dom0less_devicetree.bbclass).
XEN_DOM0LESS_REQUIRED_VARFLAGS ?= "binary binary_address dtb_address \
passthrough_template ram_address memory shared_info_addr shared_mem_name \
shared_mem_role shared_memory_address shared_memory_size evtchn_name \
evtchn_link_to evtchn_port"

# The default configuration contains one Linux domain and one Zephyr domain
XEN_DOM0LESS_DOMAINS ?= "XEN_DOM0LESS_DOM_LINUX XEN_DOM0LESS_DOM_ZEPHYR"
# Ensure varflag changes propagate to recipes using XEN_DOM0LESS_DOMAINS
XEN_DOM0LESS_DOMAINS[vardeps] = "${XEN_DOM0LESS_DOMAINS}"

# Variable to enable/disable evtchn in device tree according to
# XEN_DOM0LESS_DOMAINS Configuration
# It will be set to "0" if there's only 1 domain
XEN_DOM0LESS_EVTCHN_ENABLED = "${@'1' \
    if len(d.getVar('XEN_DOM0LESS_DOMAINS').split(' ')) == 2 else '0'}"

# Define the image recipe in order to link the boot partition to the root
# filesystem
XEN_DOM0LESS_LINUX_IMAGE ?= "core-image-minimal"
XEN_DOM0LESS_DOM_LINUX[depends] ?= "virtual/kernel:do_deploy"
XEN_DOM0LESS_DOM_LINUX[bootargs] ?= "earlycon=pl011,0x9c090000 \
console=ttyAMA0 rootfstype=ext4  root=/dev/vda2 rw"
XEN_DOM0LESS_DOM_LINUX[binary] ?= "${KERNEL_IMAGETYPE}-${MACHINE}.bin"
XEN_DOM0LESS_DOM_LINUX[binary_address] ?= "0x15000000"
XEN_DOM0LESS_DOM_LINUX[dtb_address] ?= "0x14FF0000"
XEN_DOM0LESS_DOM_LINUX[passthrough_template] ?= \
    "passthrough-linux.template.dts"
XEN_DOM0LESS_DOM_LINUX[cpus] ?= "2"
XEN_DOM0LESS_DOM_LINUX[memory] ?= "0x40000"
XEN_DOM0LESS_DOM_LINUX[ram_address] ?= "0x20000000"
XEN_DOM0LESS_DOM_LINUX[shared_info_addr] ?= "0x7F002000"
XEN_DOM0LESS_DOM_LINUX[shared_mem_name] ?= "shared-mem-dom-linux"
XEN_DOM0LESS_DOM_LINUX[shared_mem_role] ?= "borrower"
XEN_DOM0LESS_DOM_LINUX[shared_memory_address] ?= "0x70000000"
XEN_DOM0LESS_DOM_LINUX[shared_memory_size] ?= "0x1000000"
XEN_DOM0LESS_DOM_LINUX[evtchn_name] ?= "evtchn_0_linux"
XEN_DOM0LESS_DOM_LINUX[evtchn_link_to] ?= "evtchn_0_zephyr"
XEN_DOM0LESS_DOM_LINUX[evtchn_port] ?= "0x0B"

# Configuration to set demo autorun when system boot
XEN_DOM0LESS_DOM_LINUX_DEMO_AUTORUN ?= "1"

# Define the default Zephyr application recipe
XEN_DOM0LESS_ZEPHYR_APPLICATION ?= "zephyr-rpmsg-demo"
XEN_DOM0LESS_DOM_ZEPHYR[depends] ?= \
    "${XEN_DOM0LESS_ZEPHYR_APPLICATION}:do_deploy"
XEN_DOM0LESS_DOM_ZEPHYR[binary] ?= "${XEN_DOM0LESS_ZEPHYR_APPLICATION}.bin"
XEN_DOM0LESS_DOM_ZEPHYR[binary_address] ?= "0x11000000"
XEN_DOM0LESS_DOM_ZEPHYR[dtb_address] ?= "0x10FF0000"
XEN_DOM0LESS_DOM_ZEPHYR[passthrough_template] ?= \
    "passthrough-zephyr.template.dts"
XEN_DOM0LESS_DOM_ZEPHYR[cpus] ?= "2"
XEN_DOM0LESS_DOM_ZEPHYR[memory] ?= "0x40000"
XEN_DOM0LESS_DOM_ZEPHYR[ram_address] ?= "0x30000000"
XEN_DOM0LESS_DOM_ZEPHYR[shared_info_addr] ?= "0x7F001000"
XEN_DOM0LESS_DOM_ZEPHYR[shared_mem_name] ?= "shared-mem-dom-zephyr"
XEN_DOM0LESS_DOM_ZEPHYR[shared_mem_role] ?= "owner"
XEN_DOM0LESS_DOM_ZEPHYR[shared_memory_address] ?= "0x70000000"
XEN_DOM0LESS_DOM_ZEPHYR[shared_memory_size] ?= "0x1000000"
XEN_DOM0LESS_DOM_ZEPHYR[evtchn_name] ?= "evtchn_0_zephyr"
XEN_DOM0LESS_DOM_ZEPHYR[evtchn_link_to] ?= "evtchn_0_linux"
XEN_DOM0LESS_DOM_ZEPHYR[evtchn_port] ?= "0x0A"
XEN_DOM0LESS_DOM_ZEPHYR[extra] ?= "mpu;"

python() {
    # Validate the domain configuration
    if not d.getVar('XEN_DOM0LESS_DOMAINS').strip():
        raise bb.parse.SkipRecipe('XEN_DOM0LESS_DOMAINS must contain at '
                                  'least one domain')

    if not d.getVar('XEN_DOM0LESS_LINUX_IMAGE'):
        raise bb.parse.SkipRecipe('XEN_DOM0LESS_LINUX_IMAGE must contain a '
                                  'valid Linux image name')

    if not d.getVar('XEN_DOM0LESS_ZEPHYR_APPLICATION'):
        raise bb.parse.SkipRecipe('XEN_DOM0LESS_ZEPHYR_APPLICATION must '
                                  'contain a valid Zephyr application name')

    required_varflags = d.getVar('XEN_DOM0LESS_REQUIRED_VARFLAGS').split(' ')
    for dom in d.getVar('XEN_DOM0LESS_DOMAINS').split(' '):
        varflags = d.getVarFlags(dom).keys()
        missing = set(required_varflags) - set(varflags)
        if len(missing) > 0:
            raise bb.parse.SkipRecipe(f"{dom} is missing the following "
                                      f"varflags: {missing}")
}
