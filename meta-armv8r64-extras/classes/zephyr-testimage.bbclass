# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

# This class inherits testimage and additionally ensures that the test data
# and image manifest files are generated when using Zephyr recipes.

inherit testimage rootfs-postcommands
TEST_SUITES = "zephyr_v8r64"

python do_testdata_write() {
    bb.build.exec_func("write_image_test_data", d)

    # testimage requires an image manifest file, so create an empty one
    fname = os.path.join(d.getVar('DEPLOY_DIR_IMAGE'), (d.getVar('IMAGE_LINK_NAME') + ".manifest"))
    open(fname, 'w').close()
}
addtask do_testdata_write before do_testimage after do_deploy
