# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT

inherit testimage

TEST_SUITES = " xen \
    ${@bb.utils.contains('XEN_DOM0LESS_DOMAINS', 'XEN_DOM0LESS_DOM_LINUX', \
                         'linuxboot sysinfo basictests linuxlogin ptest_docker', \
                         '', d)} \
    ${@bb.utils.contains('XEN_DOM0LESS_DOMAINS', 'XEN_DOM0LESS_DOM_ZEPHYR', \
                         'zephyr_v8r64', '', d)} \
    ${@bb.utils.contains('XEN_DOM0LESS_DOMAINS', \
                         'XEN_DOM0LESS_DOM_ZEPHYR XEN_DOM0LESS_DOM_LINUX', \
                         'rpmsg', '', d)}"
IMAGE_INSTALL:append = " openssh-sshd basic-tests-ptest docker-tests-ptest"
DISTRO_FEATURES:append = " ptest"

python() {
    d.setVarFlag('FVP_CONSOLES', 'xen', 'terminal_0')
    d.setVarFlag('FVP_CONSOLES', 'zephyr_v8r64', 'terminal_1')
    d.setVarFlag('FVP_CONSOLES', 'default', 'terminal_2')
}
