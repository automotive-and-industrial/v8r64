<!--
# Copyright (c) 2022, Arm Limited.
#
# SPDX-License-Identifier: MIT
-->

# Armv8-R AArch64 Software Stack

The Armv8-R AArch64 software stack is a reference implementation of the
[Armv8-R AArch64][1] architecture on the [Armv8-R AEM FVP][2] model.
For more details on the Armv8-R AArch64 architecture, please see
[Arm Architecture Reference Manual Supplement - Armv8, for Armv8-R AArch64 architecture profile][3].
The [Fast Models FVP Reference Guide][4] provides the necessary details for the
Armv8-R AEM (Architecture Envelope Model) FVP (Fixed Virtual Platforms) model.

[1]: https://developer.arm.com/Architectures/R-Profile%20Architecture
[2]: https://developer.arm.com/tools-and-software/simulation-models/fixed-virtual-platforms/arm-ecosystem-models
[3]: https://developer.arm.com/documentation/ddi0600/ac
[4]: https://developer.arm.com/docs/100966/latest

## Documentation

The project's documentation can be browsed at
<https://armv8r64-refstack.docs.arm.com>.

To build a local version of the documentation, you will need [Sphinx][5]
installed in your work environment. The guidance at [Installing Sphinx][6]
provides detailed information.

[5]: https://www.sphinx-doc.org/
<!-- inclusivity-exception -->
[6]: https://www.sphinx-doc.org/en/master/usage/installation.html

The following commands can be used to generate an HTML version of the
documentation under `public/`.

    sudo apt-get install python3-pip
    pip3 install -U -r documentation/requirements.txt
    sphinx-build -b html -a -W documentation public

To render and explore the documentation, simply open `public/index.html` in a
web browser.

## Repository License

The repository's standard license is the MIT license, under which most of the
repository's content is provided. Exceptions to this standard license relate to
files that represent modifications to externally licensed works (for example,
patch files). These files may therefore be included in the repository under
alternative licenses in order to be compliant with the licensing requirements of
the associated external works.

License details can be found in the local [license](license.rst) file, or as
part of the project documentation.

Contributions to the project should follow the same licensing arrangement.


## Contact

Please see the project documentation for the list of maintainers, as well as the
process for raising issues, or receiving feedback and support.
